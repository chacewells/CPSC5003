/*
 * Aaron Wells
 * CPSC 5003, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab5;

/**
 * Hash table data type that stores integer values keyed by integer. Constructed
 * with a maximum capacity. Once the capacity is reached, new keys cannot be
 * added. Only existing keys can have their values overwritten.
 * 
 * @author Aaron Wells
 *
 */
public class HashTable {
	private int capacity; // the maximum # of elements this table can hold
	private int size; // the current size of the table
	private HashBucket[] table; // the table
	
	/**
	 * Constructs a new HashTable with the given capacity.
	 * 
	 * @param capacity The maximum # of elements this table can hold.
	 */
    public HashTable(int capacity) {
    	this.capacity = capacity;
    	this.size = 0;
    	table = new HashBucket[capacity];
    }

    /**
     * Adds the given key/value combination to the table. If the key exists,
     * its value is overwritten, and the previous value is returned. Once the
     * capacity has been reached, new keys cannot be added. Only existing keys
     * can have their values overwritten.
     * 
     * @param key The lookup key.
     * @param value The value to add.
     * @return The previous value associated with the key; otherwise -1 if the
     * key does not exist.
     * @throws IllegalStateException If the capacity has been reached, but the
     * user attempts to add a new key.
     */
	public int put(int key, int value) throws IllegalStateException {
		int loc = locate(key); // hash key for array location
		int ret = -1; // return previous value or -1 if not found
		
		if (loc == -1)
			throw new IllegalStateException("The table is full and an "
					+ "existing key could not be found!");
		
		if (table[loc] != null) {
			ret = table[loc].value;
			table[loc].value = value;
		} else {
			table[loc] = new HashBucket(key, value);
			++size;
		}
		
		return ret;
    }
	
	/**
	 * Retrieves the value associated with the given key.
	 * 
	 * @param key The key to lookup.
	 * @return The value associated with the key; or -1 if the key does not
	 * exist.
	 */
    public int get(int key) {
    	int loc = locate(key); // the lookup location
    	
    	return (loc != -1 && table[loc] != null)
			? table[loc].value
			: -1;
    }
    
    /**
     * Whether this key exists in the table.
     * 
     * @param key The key to lookup. 
     * @return True if the key exists in the table; false otherwise.
     */
    public boolean contains(int key) {
    	if (size == 0)
    		return false;
    	
    	int loc = locate(key); // the lookup location
    	return loc != -1 && table[loc] != null;
    }
    
    /**
     * The current number of key/value pairs contained in the table.
     * 
     * @return The size of the hash table.
     */
    public int size() {
    	return size;
    }
    
    /**
     * Whether the hash table is empty.
     * 
     * @return True if size() == 0; false otherwise.
     */
    public boolean isEmpty() {
    	return size == 0;
    }

    // A key/value record type for storing table entries.
    private static class HashBucket {
        private final int key;
        private int value;

        /**
         *  constructs a new HashBucket with key and value
         * @param key The lookup key.
         * @param value The value to store.
         */
        public HashBucket(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    /*
     * Locates the index of the given key.<br>
     * If the key cannot be found, but space is available on the table, returns
     * the next open location.<br>
     * If the table is full returns -1, indicating no space available.<br>
     * If the key is found, returns the location of that key's HashBucket.
     */
	private int locate(int key) {
		int loc = hash(key); // location begins with the key's hash
		// Keep track of the start in case the table is full
		int startLoc = loc;
		
		HashBucket existingBucket = table[loc]; // the bucket at loc
		while (existingBucket != null) {
			if (existingBucket.key == key)
				return loc;
		
			loc = (loc + 1) % capacity;
			if (loc == startLoc)
				return -1; // no location available
			existingBucket = table[loc];
		}
			
		return loc;
	}

	// calculate the hash of the given key
	private int hash(int key) {
		return key % capacity;
	}
}
