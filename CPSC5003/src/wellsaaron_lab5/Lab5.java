/*
 * Aaron Wells
 * CPSC 5003, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab5;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Tests the performance and operations of HashTable.
 * 
 * @author Aaron Wells
 *
 */
public class Lab5 {
	// the HashTable capacity for both tests
	private static final int CAPACITY = 4093;

	/**
	 * Runs the tests.
	 * @param args no command line args expected
	 */
	public static void main(String[] args) {
		testPerformance();
		testPutGetAndContains();
	}
	
	// Displays data on how performance degrades over the last 100 entries
	private static void testPerformance() {
		// how many key/value pairs to insert initially
		int initialEntries = 3993;
		HashTable ht = new HashTable(CAPACITY); // The hash table to test
		Pair[] pairsToPut = generateUniquePairs(CAPACITY); // the pairs to add
		Pair pair; // the current pair to add
		int currentPair = 0; // the current key/value pair index
		
		for (; currentPair < initialEntries; ++currentPair) {
			pair = pairsToPut[currentPair];
			ht.put(pair.key, pair.value);
		}
		
		System.out.println("ht.size() => " + ht.size());
		
		int testEnd; // step to end current performance test
		long start, end; // track current test's performance
		double duration; // the current test's duration

		System.out.println("% full\tms");
		while (ht.size() < CAPACITY) {
			start = System.nanoTime();
			for (testEnd = currentPair + 10;
					currentPair < testEnd;
					++currentPair) {
				pair = pairsToPut[currentPair];
				ht.put(pair.key, pair.value);
			}
			end = System.nanoTime();
			
			duration = (end - start) / 1_000.0;
			System.out.printf("%.4f\t%.6f%n", 
					(ht.size() / (double)CAPACITY) * 100.0,
					duration);
		}
	}
	
	// Functional tests for put(), get(), and contains()
	private static void testPutGetAndContains() {
		int initialEntries = CAPACITY - 100; // start with 100 short of capacity
		HashTable ht = new HashTable(CAPACITY); // the hash table to test
		Pair[] pairs = generateUniquePairs(initialEntries); // initial k/v pairs
		
		for (Pair pair: pairs)
			ht.put(pair.key, pair.value);

		// specific pairs to add
		Pair[] additionalPairs = additionalPairs();
		for (Pair pair: additionalPairs)
			ht.put(pair.key, pair.value);
		
		// two pairs to test on ht
		Pair[] testPairs = getTestPairs();
		System.out.println("\n**TESTING GET FOR PAIRS: " 
				+ Arrays.toString(testPairs) 
				+ "**");
		for (Pair p: testPairs)
			testGet(ht, p.key, p.value);
		
		// additional keys not in ht
		UniquePairGenerator missingKeyFinder = missingKeyFinder(
				pairs,
				additionalPairs);
		
		System.out.println("\n**TESTING GET FOR ADDITIONAL MISSING KEYS**");
		for (int i = 0; i < 2; ++i)
			testGet(ht, missingKeyFinder.nextInt(), -1);
		
		System.out.println("\n**TESTING CONTAINS FOR PAIRS: "
				+ Arrays.toString(testPairs)
				+ "**");
		for (Pair p: testPairs)
			testContains(ht, p.key, true);
	}
	
	// helper that builds an array of expected key/value pairs for 50184 and 77
	// works off of data contained in additionalPairs()
	private static Pair[] getTestPairs() {
		List<Integer> filter = Arrays.asList(50184, 77); // extract these pairs
		Pair[] testPairs = new Pair[2]; // An array containing the pairs
		int j = 0; // track the index filled in testPairs
		for (Pair pair: additionalPairs())
			if (filter.contains(pair.key))
				testPairs[j++] = pair;
		
		return testPairs;
	}
	
	// stops the test if actual does not match expected
	private static void testContains(HashTable ht, int key, boolean expected) {
		boolean actual = ht.contains(key); // result of contains()
		if (actual != expected) {
			System.err.printf("ht.contains(%d): expected %b; actual %b!%n",
					key,
					expected,
					actual);
			System.exit(1);
		}
		
		System.out.printf("ht.contains(%d, args): %b == %b; passed%n",
				key,
				expected,
				actual);
	}
	
	// stops the test if actual does not match expected
	private static void testGet(HashTable ht, int key, int expected) {
		int actual = ht.get(key); // result of get()
		if (actual != expected) {
			System.err.printf("ht.get(%d): expected %d; actual %d!%n",
					key,
					expected,
					actual);
			System.exit(1);
		}
		
		System.out.printf("ht.get(%d, args): %d == %d; passed%n",
				key,
				expected,
				actual);
	}
	
	// supplies a generator that will not repeat keys from pairContainers
	private static UniquePairGenerator missingKeyFinder(
			Pair[]... pairContainers) {
		// The keys already in the hash table
		Set<Integer> unique = new HashSet<>();
		for (Pair[] pairs: pairContainers)
			for (Pair pair: pairs)
				unique.add(pair.key);
		
		// the resulting number generator
		UniquePairGenerator gen = new UniquePairGenerator();
		gen.uniqueIntegers = unique;
		
		return gen;
	}
	
	// Generates a set of key/value pairs specified in the assignment
	private static Pair[] additionalPairs() {
		// the key/value pairs specified in the assignment
		return new Pair[] {
			new Pair(1179  , 120),
			new Pair(9702  , 121),
			new Pair(7183  , 122),
			new Pair(50184 , 123),
			new Pair(4235  , 124),
			new Pair(644   , 125),
			new Pair(77    , 126),
			new Pair(3077  , 127),
			new Pair(23477 , 128),
			new Pair(90777 , 129)
		};
	}
	
	// a random set of unique key/value pairs
	private static Pair[] generateUniquePairs(int n) {
		// generates the data set
		UniquePairGenerator gen = new UniquePairGenerator();
		Pair[] pairs = new Pair[n]; // the data set to return
		for (int i = 0; i < n; ++i) {
			pairs[i] = gen.nextPair();
		}
		
		return pairs;
	}
	
	// a utility that generates unique random integers
	private static class UniquePairGenerator {
		// a cache to ensure uniqueness
		private Set<Integer> uniqueIntegers = new HashSet<>();
		
		// a new Pair consisting of unique key and value integers
		Pair nextPair() {
			return new Pair(nextInt(), nextInt());
		}
		
		// a random integer not previously generated by this utility
		int nextInt() {
			Random random = new Random(); // supplies the random int
			int unique; // the integer to generate
			do {
				unique = Math.abs(random.nextInt());
			} while (uniqueIntegers.contains(unique));
			uniqueIntegers.add(unique);
			
			return unique;
		}
	}
	
	// a simple key/value record type
	private static class Pair {
		int key; // the key
		int value; // the value
		
		// constructs a new Pair with a key and value
		Pair(int key, int value) {
			this.key = key;
			this.value = value;
		}
		
		@Override
		public String toString() {
			return String.format("Pair(key=%d,value=%d)", key, value);
		}
	}

}
