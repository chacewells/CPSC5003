/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package hashing_inclass;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class HashPlay {

	public HashPlay(String s) {
		key = s;
	}
	
	public int hashCode() {
		int hash = 0;
		
		if (key != null && !key.isEmpty()) {
			int mid = key.length() / 2, len = key.length();
			hash += 100 * key.charAt(0) + 10 * key.charAt(mid) + key.charAt(len - 1);
		}
		
		return hash;
	}
	
	public static void main(String[] args) throws IOException {
		Files.newBufferedReader(Paths.get("hobbit-words.txt"))
		.lines()
		.map(HashPlay::new)
		.forEach(System.out::println);
//		HashPlay hp = new HashPlay("ant");
//		System.out.println(hp.hashCode());
	}
	
	@Override
	public String toString() {
		return String.format("HashPlay(key='%s', hashCode=%d)",
				key,
				hashCode());
	}
	
	private String key;
}
