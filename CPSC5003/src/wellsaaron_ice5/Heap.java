/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_ice5;

import java.util.EmptyStackException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Heap implements PriorityQueueADT<Integer> {
	public static void heapsort(Integer[] data) {
		Heap heap = new Heap(data);
		
		while (!heap.empty())
			heap.dequeue();
		
		reverse(data);
	}
	
	private static void reverse(Integer[] data) {
		for (int i = 0, j = data.length - 1; i < j; ++i, --j) {
			Integer swap = data[i];
			data[i] = data[j];
			data[j] = swap;
		}
	}
	
	private Integer[] data;
	private int size;
	private int capacity;
	
	// takes the capacity
	// creates a min heap based on the capacity
	public Heap(int capacity) {
		this.size = 0;
		this.capacity = capacity;
		this.data = new Integer[capacity];
	}
	
	public Heap(Integer[] data) {
		this(data, data.length);
	}
	
	public Heap(Integer[] data, int size) {
		this.capacity = data.length;
		this.size = size;
		this.data = data;
		heapify();
	}

	@Override
	public boolean empty() {
		return size == 0;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Heap[");
		
		sb.append(IntStream.range(0, size)
				.mapToObj(i -> data[i])
				.map(String::valueOf)
				.collect(Collectors.joining(" ")));
		
		sb.append("];size=" + size);
		
		return sb.toString();
	}
	
	@Override
	public void enqueue(Integer element) {
		if (size == capacity)
			throw new IllegalStateException("The heap is full");
		
		// add to end of array
		data[size++] = element; // FIXME check capacity
		// increase size
		// create a recursive helper, percolateUp,
		//   that allows you puts the inserted val 
		//   in the right place
		percolateUp(size - 1);
	}
	
	@Override
	public Integer peek() throws EmptyStackException {
		if (empty())
			throw new EmptyStackException();
		return data[0];
	}

	@Override
	public Integer dequeue() throws EmptyStackException {
		Integer ret = peek();
		
	    // get last val in heap, copy value to index 0
	    // decrease size
		data[0] = data[--size];
		data[size] = ret;
		
	    // create a recursive helper, percolateDown,
	    //   that allows you move the removed val 
	    //   in the right place
		percolateDown(0);
		
		return ret;
	}
	
	private void heapify() {
		// start in the middle (all children and nodes to the right (same level) are heaps)
		for (int i = size / 2;
				i >= 0; --i)
			percolateDown(i);
	}

	private boolean hasLeft(int parentIndex) {
		return left(parentIndex) < size;
	}

	private boolean hasRight(int parentIndex) {
		return right(parentIndex) < size;
	}

	private int left(int parentIndex) {
		return parentIndex * 2 + 1;
	}

	private int right(int parentIndex) {
		return left(parentIndex) + 1;
	}

	private int parent(int childIndex) {
		return (childIndex - 1) / 2;
	}

	private void percolateUp(int index) {
		if (index > 0) {
			int p = parent(index);
			if (data[p] > data[index]) {
				Integer temp = data[p];
				data[p] = data[index];
				data[index] = temp;
				percolateUp(p);
			}
		}
	}
	
// my solution
//	private void percolateDown(int index) {
//		if (index < size) {
//			Integer p = data[index];
//			if (hasLeft(index) && hasRight(index)) {
//				Integer left = data[left(index)];
//				Integer right = data[right(index)];
//				
//				if (left <= right && left < p) {
//					swap(index, left(index));
//					percolateDown(left(index));
//				} else if (right < p) {
//					swap(index, right(index));
//					percolateDown(right(index));
//				}
//			} else if (hasLeft(index)) {
//				Integer left = data[left(index)];
//				if (p > left) {
//					swap(index, left(index));
//					percolateDown(left(index));
//				}
//			} else if (hasRight(index)) {
//				Integer right = data[right(index)];
//				if (p > right) {
//					swap(index, right(index));
//					percolateDown(right(index));
//				}
//			}
//		}
//	}
	
	// K Lundeen's solution
	private void percolateDown(int index) {
		if (hasLeft(index)) {
			int child = left(index);
			if (hasRight(index)) {
				int r = right(index);
				if (data[r] < data[child])
					child = r; // right child is the min child
			}
			if (data[child] < data[index]) {
				swap(child, index);
				percolateDown(child);
			}
		}
	}
	
	private void swap(int indexA, int indexB) {
		Integer swap = data[indexA];
		data[indexA] = data[indexB];
		data[indexB] = swap;
	}

}
