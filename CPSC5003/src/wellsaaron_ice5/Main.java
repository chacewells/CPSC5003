/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_ice5;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Main {
	
	private static final Random rand = new Random();
	
	public static void main(String[] args) {
		for (int n = 5_000; n < 100_000_000; n *= 2) {
			long start = System.nanoTime();
			heapsortTest(n, 10_000, false);
			long end = System.nanoTime();
			System.out.println(n + ": " + (end - start) / 1_000_000_000.0 + " seconds");
		}
	}
	
	public static void heapsortTest(int size, int range, boolean print) {
		Integer[] data = new Integer[size];
		for (int i = 0; i < size; ++i)
			data[i] = rand.nextInt(range);
		Heap.heapsort(data);
		if (print) {
			System.out.println("sorted: ");
			for (Integer elem: data)
				System.out.print(elem + " ");
			
			System.out.println();
		}
	}
	
	public static void heapTest() {
		final int heapSize = 1000;
		Heap h = new Heap(heapSize);
		for (int i = 0; i < heapSize; ++i) {
			h.enqueue(rand.nextInt(heapSize * 100));
			if (i % 4 == 0)
				h.dequeue();
		}
		
		List<Integer> dequeueResults = new LinkedList<>();
		
		System.out.println("Initialized heap:");
		System.out.println(h);
		System.out.println();
		
		while (!h.empty()) {
			System.out.println("Peeking: " + h.peek());
			Integer value = h.dequeue();
			dequeueResults.add(value);
			System.out.println("Dequeueing; " + value);
			
			System.out.println("Heap: " + h);
			System.out.println();
		}
		
		System.out.println("Dequeue results: " + dequeueResults);
		System.out.println("Heap order? " + isSorted(dequeueResults));
	}
	
	private static boolean isSorted(List<Integer> list) {
		Integer last = null, curr = null;
		
		for (Iterator<Integer> itr = list.iterator(); itr.hasNext();) {
			if (last == null) {
				last = itr.next();
				continue;
			}
			
			curr = itr.next();
			if (curr < last)
				return false;
			
			last = curr;
		}
		
		return true;
	}

}
