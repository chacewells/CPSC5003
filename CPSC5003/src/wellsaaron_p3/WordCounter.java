/*
 * Aaron Wells
 * CPSC 5003, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p3;

import java.util.function.Function;

/**
 * Implements word counting as a hash table.
 * 
 * @author Aaron Wells
 *
 */
public class WordCounter {
	/**
	 * Default capacity of prime number ~20
	 */
	public static final int DEFAULT_CAPACITY = 23;
	
	// filled entries in table; useful for determining emptiness
	private int bucketCount;
	private Bucket[] table; // the hash table
	
	/**
	 * Initializes a new WordCounter with the given capacity.
	 * 
	 * @param capacity Size of hash table backing WordCounter.
	 */
    public WordCounter(int capacity) {
    	bucketCount = 0;
    	this.table = new Bucket[nextPrime(capacity)];
    }
    
    /**
     * Initializes a new WordCounter with a capacity of DEFAULT_CAPACITY.
     */
    public WordCounter() {
    	this(DEFAULT_CAPACITY);
    }
    
    /**
     * @return The capacity; i.e. the value hashes are calculated on.
     */
    public int getCapacity() {
    	return table.length;
    }
    
    /**
     * @return The number of unique words.
     */
    public int getUniqueWordCount() {
    	return traverseCount(b -> 1);
    }
    
    /**
     * @return The total count of words.
     */
    public int getTotalWordCount() {
    	return traverseCount(b -> b.count);
    }
    
    /**
     * @return True if the number of entries is 0; false otherwise.
     */
    public boolean isEmpty() {
    	return bucketCount == 0;
    }
    
	/**
	 * Increments the count of the given word.
	 * 
	 * @param word The word for which to increment the count.
	 * @return The updated word count.
	 */
	public int incrementWordCount(String word) {
		int hashCode = word.hashCode(); // the hash code
		int location = Math.abs(hashCode % table.length); // location in table
		int count;

		if (table[location] == null) {
			table[location] = new Bucket(word, count = 1);
			++bucketCount;
		} else if (word.equals(table[location].word)) {
			count = ++table[location].count;
		} else {
			Bucket curr = table[location];
			while (curr.next != null) {
				if (word.equals(curr.next.word)) {
					return ++curr.next.count;
				}

				curr = curr.next;
			}

			curr.next = new Bucket(word, count = 1);
			++bucketCount;
		}
		
		resize();
		
		return count;
	}

	/**
     * @param word The word to count.
     * @return The word count.
     */
    public int getWordCount(String word) {
        int hashCode = word.hashCode(); // the hash code
        int location = Math.abs(hashCode % table.length); // the table location
        for (Bucket curr = table[location]; curr != null; curr = curr.next)
        	if (word.equals(curr.word))
        		return curr.count;
        
        return 0;
    }
    
    /**
     * Remove the word from the hash table.
     * @param word The word to remove.
     */
    public void removeWord(String word) {
        int hashCode = word.hashCode(); // the hash code
        int location = Math.abs(hashCode % table.length); // the table location
        
        if (table[location] != null) {
        	if (word.equals(table[location].word))
        		table[location] = table[location].next;
        	else {
	        	Bucket prev = null; // previous reference
	        	Bucket curr = table[location]; // current reference
	        	while (curr.next != null) {
	        		prev = curr;
	        		curr = curr.next;
	        		if (word.equals(curr.word)) {
	        			prev.next = curr.next;
	        			--bucketCount;
	        			break;
	        		}
	        	}
        	}
        	
        	resize();
        }
    }

    // Helper method for public count methods. Builds count according to the
    // strategy provided by countFn
    private int traverseCount(Function<Bucket,Integer> countFn) {
		int count = 0; // the count
		for (Bucket bucket: table)
			for (Bucket curr = bucket; curr != null; curr = curr.next)
				count += countFn.apply(curr);
		
		return count;
	}

    // helper class acts as a linked list node and key/value pair for word count
	private static class Bucket {
        private String word;
        private int count;
        private Bucket next;
        
        // Bucket constructor
        Bucket(String word, int count) {
        	this.word = word;
        	this.count = count;
        	this.next = null;
        }
        
        @Override
        public String toString() {
        	return "Bucket(word="+word+";count="+count+")";
        }
    }
	
	// get the next prime number after x
	private static int nextPrime(int x) {
		int i;
		for (i = 0; i < primes.length && x > primes[i]; ++i);

		return primes[i];
	}
	
	// list of known prime numbers
	private static final int[] primes = {
	    11, 13, 17, 19, 23, 29, 31, 37, 43, 53, 67, 79, 97, 107, 131, 157, 191,
	    223, 269, 331, 389, 461, 557, 673, 797, 967, 1151, 1381, 1657, 1979,
	    2377, 2851, 3433, 4111, 4931, 5923, 7103, 8513, 10211, 12251, 14699,
	    17657, 21169, 25409, 30491, 36583, 43889, 52667, 63199, 75853, 91009,
	    109211, 131059, 157259, 188707, 226451, 271753, 326087, 391331, 469583,
	    563489, 676171, 811411, 973691, 1168451, 1402123, 1682531, 2019037,
	    2422873, 2907419, 3488897, 4186673, 5024009, 6028807, 7234589, 8681483,
	    10417769, 12501331, 15001603, 18001909, 21602311, 25922749, 31107317,
	    37328761, 44794513, 53753431, 64504081, 77404907, 92885893, 111463049,
	    133755659, 160506817, 192608173, 231129781, 277355759, 332826869,
	    399392243, 479270713, 575124829, 690149821, 828179753, 993815743
	};
	
	// determine the current load factor
	private double loadFactor() {
		return bucketCount / (double)table.length;
	}
	
	// conditionally resize the table to conform to a load tolerance
	private void resize() {
		final double loadMax = 0.75;
		final double loadMin = 0.3;
		
		if (loadFactor() > loadMax)
			grow();
		else if (loadFactor() < loadMin)
			shrink();
	}
	
	// double the array size
	private void grow() {
		rehash(nextPrime(table.length * 2));
	}
	
	// halve the array size
	private void shrink() {
		rehash(nextPrime(table.length / 2));
	}
	
	// rehash the array with a new capacity
	private void rehash(int newCapacity) {
		Bucket[] oldTable = table;
		table = new Bucket[newCapacity];
		
		for (Bucket buck: oldTable)
			for (Bucket curr = buck; curr != null; curr = curr.next)
				insert(curr);
	}
	
	// copy a bucket's contents into the table
	private void insert(Bucket bucket) {
		int hashCode = bucket.word.hashCode();
		int location = Math.abs(hashCode % table.length);
		
		if (table[location] == null)
			table[location] = new Bucket(bucket.word, bucket.count);
		else {
			Bucket curr;
			for (curr = table[location]; curr.next != null; curr = curr.next);
			
			// copy bucket data, otherwise we could end up corrupting
			// some nodes
			curr.next = new Bucket(bucket.word, bucket.count);
		}
	}
}