package wellsaaron_p3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class WordCounterTest {

	public static void main(String[] args) throws IOException {
//		testGrow();
		testShrink();
	}
	
	private static void testGrow() throws IOException {
		WordCounter wc = new WordCounter();
		
		System.out.println("Inserting lots 'o words to check the growing"
				+ " functionality...");
		Files.newBufferedReader(Paths.get("hobbit-words.txt"))
			.lines()
			.forEach(w -> {
				System.out.println("Inserting: " + w);
				wc.incrementWordCount(w);
			});
	}
	

	private static void testShrink() throws IOException {
		final int primeTime = 4186673;
		WordCounter wc = new WordCounter(primeTime);
		
		Files.newBufferedReader(Paths.get("hobbit-words.txt"))
			.lines()
			.forEach(wc::incrementWordCount);
		
		System.out.println("Removing lots 'o words to check the growing"
				+ " functionality...");
		Files.newBufferedReader(Paths.get("hobbit-words.txt"))
			.lines()
			.forEach(w -> {
				System.out.println("Removing: " + w);
				wc.removeWord(w);
			});
	}

}
