/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package sorts_inclass;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.IntStream;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Sort {
	private static final int QSORT_MIN = 4;
	
	enum SortAlgorithm {
		selectionsort(Sort::selectionsort),
		insertionsort(Sort::insertionsort),
		mergesort(Sort::mergesort),
		quicksort(Sort::quicksort);
		
		private Consumer<Integer[]> sortFn;
		
		private SortAlgorithm(Consumer<Integer[]> sortFn) {
			this.sortFn = sortFn;
		}
		
		public void doSort(Integer[] arr) {
			sortFn.accept(arr);
		}
	}
	
	public static void main(String[] args) {
		EnumSet.allOf(SortAlgorithm.class)
			.forEach(Sort::testSort);
	}
	
	private static void testSort(SortAlgorithm algorithm) {
		System.out.println("\nTesting &" + algorithm.name());
		System.out.println("======================");
		Integer[] arr = getRandomData();

		System.out.println("arr before: " + Arrays.toString(arr));
		algorithm.doSort(arr);
		System.out.println("arr after:  " + Arrays.toString(arr));
	}
	
	private static Integer[] getRandomData() {
		final int elems = 11, bound = 100;
		Random rand = new Random();
		return IntStream.range(0, elems)
			.mapToObj(i -> rand.nextInt(bound))
			.toArray(i -> new Integer[elems]);
	}

	public static void insertionsort(Integer[] array) {
		insertionsort(array, 0, array.length - 1);
	}
	
	private static void insertionsort(Integer[] array, int first, int last) {
		for (int i = first; i <= last; i++) {
			for (int j = i; j > 0 && array[j - 1] > array[j]; --j) {
				int temp = array[j];
				array[j] = array[j - 1];
				array[j - 1] = temp;
			}
		}
	}
	
	public static void selectionsort(Integer[] arr) {
		for (int i = 0; i < arr.length - 1; ++i) {
			// find smallest value and swap with current value
			int min = i;
			for (int j = i + 1; j < arr.length; ++j) {
				if (arr[j] < arr[min])
					min = j;
			}
			
			swap(arr, i, min);
		}
	}
	
	public static void mergesort(Integer[] a) {
		Integer[] temp = new Integer[a.length];
		mergesort(a, temp, 0, a.length - 1);
	}
	
	private static void mergesort(Integer[] a, Integer[] temp, int first, int last) {
		if (first < last) { // 2+ elements to sort
			int mid = (first + last) / 2;
			mergesort(a, temp, first, mid);
			mergesort(a, temp, mid + 1, last);
			merge(a, temp, first, mid, last);
		}
	}

	private static void merge(Integer[] a, Integer[] temp, int first, int mid, int last) {
		int begin1 = first;
		int end1 = mid;
		int begin2 = mid + 1;
		int end2 = last;
		int index = first;
		
		while (begin1 <= end1 && begin2 <= end2)
			if (a[begin1] < a[begin2])
				temp[index++] = a[begin1++];
			else
				temp[index++] = a[begin2++];
				
		
		while (begin1 <= end1)
			temp[index++] = a[begin1++];
		while (begin2 <= end2)
			temp[index++] = a[begin2++];
		
		// copy form temp back to a
		for (int i = first; i <= last; ++i)
			a[i] = temp[i];
	}
	
	public static void quicksort(Integer[] a) {
		quicksort(a, 0, a.length - 1);
	}
	
	

	private static void quicksort(Integer[] a, Integer first, Integer last) {
		if ((last - first + 1) < QSORT_MIN)
			insertionsort(a, first, last);
		else {
			Integer pivot = partition(a, first, last);
			quicksort(a, first, pivot - 1);
			quicksort(a, pivot + 1, last);
		}
	}
	
	private static int partition(Integer[] a, int first, int last) {
		int mid = (first + last) / 2;
		sort3(a, first, mid, last);
		
		// swap a[mid] and a[last-1]
		int p = last - 1;  // sort3 already sorted a[last]
		int temp = a[mid];
		a[mid] = a[p];
		a[p] = temp;
		
		int pv = a[p];
		int l = first + 1;
		int r = last - 2;
		boolean done = false;
		while (!done) {
			while (a[l] < pv)
				l++;
			while (a[r] > pv)
				r--;
			if (l < r) {
				temp = a[l];
				a[l++] = a[r];
				a[r--] = temp;
			} else {
				done = true;
			}
		}
		a[p] = a[l];
		a[l] = pv;
		return l;
	}

	private static void sort3(Integer[] a, int one, int two, int three) {
		if (a[one] > a[two]) {
			int temp = a[one];
			a[one] = a[two];
			a[two] = temp;
		}
		if (a[two] > a[three]) {
			int temp = a[two];
			a[two] = a[three];
			a[three] = temp;
		}
		if (a[one] > a[two]) {
			int temp = a[one];
			a[one] = a[two];
			a[two] = temp;
		}
	}


	// swap 2 elems in an array
	private static void swap(Integer[] arr, int a, int b) {
		int temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}

}
