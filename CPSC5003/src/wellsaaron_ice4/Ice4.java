/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_ice4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Ice4 {
	private static final int N_DEFAULT = 100;
	private static final double NANOS_PER_SEC = 1_000_000_000.0;
	private static final int N_RUNS = 5;
	private static BufferedReader console;
	
	public static void main(String[] args) {
		initConsole();
		
		int n = getNFromUser();
		int val = getValFromUser();
		int limit = n < 3200 ? 3200 : n;

		for (; n <= limit; n *= 2 ) {
			double tSum = 0.0;
			for (int run = 0; run < N_RUNS; ++run) {
				double t = getFooTimeSeconds(n, val);
				tSum += t;
			}
			
			double tAvg = tSum / N_RUNS;
			System.out.printf("average foo(n=%d, val=%d) took %.6f seconds\n", n, val, tAvg);
		}
	}
	
	private static void initConsole() {
		console = new BufferedReader(new InputStreamReader(System.in));
	}
	
	private static int getNFromUser() {
		return getNumberFromUser("n");
	}
	
	private static int getValFromUser() {
		return getNumberFromUser("val");
	}
	
	private static int getNumberFromUser(String valueName) {
		System.out.print("Enter a value of " + valueName + " to test: ");
		try {
			String userInput = console.readLine();
			int number = Integer.parseInt(userInput);
			return number;
		} catch (IOException|NumberFormatException e) {
			System.err.println("Couldn't parse user input; defaulting to " + N_DEFAULT);
			return N_DEFAULT;
		}
	}
	
	private static double getFooTimeSeconds(long n, long val) {
		final long startTime = System.nanoTime();
		foo(n, val);
		final long endTime = System.nanoTime();
		return (endTime - startTime) / NANOS_PER_SEC;
	}
	
	/**
	 * Mystery function.
	 * @param n   mystery argument 1
	 * @param val mystery argument 2
	 */
	private static void foo(long n, long val) {
	    long b = 0;
	    long c = 0;
	    for (long j = 4; j < n; j++) {
	        for (long i = 1; i < j; i++) {
	            b = b * val;
	            for (long k = 1; k <= n; ++k) {
	                c = b + c;
	            }
	        }
	    }
	}
	
}
