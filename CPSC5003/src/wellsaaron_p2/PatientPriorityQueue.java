/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p2;

import java.util.ArrayList;

/**
 * Hospital triage system implemented using a heap.
 * @author bc3soln
 */
public class PatientPriorityQueue {
    private ArrayList<Patient> patients; // heap property is always satisfied
    private int nextPatientNumber;       // num assigned to next added patient

    /**
     * Creates an empty triage system with no patients.
     */
    public PatientPriorityQueue() {
        this.patients = new ArrayList<Patient>();
        this.nextPatientNumber = 1;
    }

    /**
     * Gets the list of patients currently in the waiting room
     * @return the list of patients that have not been called
     */
    public ArrayList<Patient> getPatientList() {
        return patients;
    }

    /**
     * Add a patient to this priority queue. The patient will be prioritized
     * based on his/her priorityCode. Lower numbered priorities are placed
     * ahead of higher number priorities, otherwise the patients are placed in
     * order of arrival.
     * 
     * @param priorityCode One of 1, 2, 3, or 4
     * @param patientName The patient's name
     */
    public void addPatient(int priorityCode, String patientName) {
    	Patient nextPatient = new Patient(
    			priorityCode, 
    			nextPatientNumber++, 
    			patientName);
    	
    	patients.add(nextPatient);
    	percolateUp(patients.size() - 1);
    }
    
    /**
     * Return, but do not remove, the next patient.
     * @return The next patient.
     */
    public Patient peek() {
    	return patients.get(0);
    }

    /**
     * Return and remove the next patient from the queue.
     * @return The next patient.
     */
    public Patient dequeue() {
    	Patient ret = peek();
    	Patient lastPatient = patients.remove(patients.size() - 1);
    	if (patients.size() > 0) {
	    	patients.set(0, lastPatient);
	    	percolateDown(0);
    	}
    	
    	return ret;
    }
    
    /**
     * The number of patients in the queue.
     * @return The number of patients in the queue.
     */
    public int size() {
        return patients.size();
    }

    // Moves the Patient at index ahead in the queue until it is placed in its
    // proper order.
	private void percolateUp(int index) {
		if (index > 0) {
			int p = parent(index);
			if (patients.get(p).compareTo(patients.get(index)) > 0) {
				swap(index, p);
				percolateUp(p);
			}
		}
	}
	
	// Moves the Patient at index back in the queue until it is behind its
	// predecessors.
	private void percolateDown(int index) {
		if (hasLeft(index)) {
			int child = left(index);
			if (hasRight(index)) {
				int r = right(index);
				if (patients.get(r).compareTo(patients.get(child)) < 0)
					child = r; // right child is the min child
			}
			if (patients.get(child).compareTo(patients.get(index)) < 0) {
				swap(child, index);
				percolateDown(child);
			}
		}
	}

	// Whether there is a left child of the parentIndex
	private boolean hasLeft(int parentIndex) {
		return left(parentIndex) < patients.size();
	}

	// Whether there is a right child of the parentIndex
	private boolean hasRight(int parentIndex) {
		return right(parentIndex) < patients.size();
	}

	// The index of the left child
	private int left(int parentIndex) {
		return parentIndex * 2 + 1;
	}

	// The index of the right child
	private int right(int parentIndex) {
		return left(parentIndex) + 1;
	}

	// The parent's index
	private int parent(int childIndex) {
		return (childIndex - 1) / 2;
	}

	// Swap two positions of the patients ArrayList.
	private void swap(int a, int b)	{
		Patient temp = patients.get(a);
		patients.set(a, patients.get(b));
		patients.set(b, temp);
	}
}
