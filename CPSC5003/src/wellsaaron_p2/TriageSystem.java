/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.EnumSet;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * An emergency room triage system. Patients are added with their priority and
 * queued up according to 1) their urgency, and 2) their order of arrival.
 * All immediate patients are treated first, then emergency, then urgent, then
 * minimal. If we're lucky, all the minimal patients will lose patience after
 * about an hour, then walk out, so we don't have to do the work of treating
 * them.
 * 
 * @author wellsaaron
 */
public class TriageSystem {
	// Greet the user and give a little guidance
    private static final String MSG_WELCOME = "Welcome to the triage system.\n"
    		+ "Use this system to enter patients with their respective\n"
    		+ "urgency, and they will be queued up to receive treatment\n"
    		+ "based on their urgency (first) and time of arrival (second).\n"
    		+ "For a list of commands and their usage, type 'help'.";
    // say goodbye
    private static final String MSG_GOODBYE = "Thanks for using the triage "
    		+ "program.\n"
    		+ "Goodbye!";
    // display a help message
    private static final String MSG_HELP = "" +
"add <priority-code> <patient-name>\n" + 
"            Adds the patient to the triage system.\r\n" + 
"            <priority-code> must be one of the 4 accepted priority codes:\n" + 
"                1. immediate 2. emergency 3. urgent 4. minimal\n" + 
"            <patient-name>: patient's full legal name (may contain spaces)\n" + 
"next        Announces the patient to be seen next. Takes into account the\n" + 
"            type of emergency and the patient's arrival order.\n" + 
"peek        Displays the patient that is next in line, but keeps in queue\n" + 
"list        Displays the list of all patients that are still waiting\n" + 
"            in the order that they have arrived.\n" + 
"load <file> Reads the file and executes the command on each line\n" + 
"help        Displays this menu\n" + 
"quit        Exits the program";
    // inform the user there are no more patients waiting
    private static final String NO_PATIENTS_WAITING = "There are no patients "
    		+ "in the waiting room.";
    
    // whether the program should not done
    private static boolean keepAsking = true;

	/**
     * Entry point of the program
     * @param args not used
     */
    public static void main(String[] args) {
        System.out.println(MSG_WELCOME);

        Scanner console = new Scanner(System.in);
        PatientPriorityQueue
            priQueue = new PatientPriorityQueue();
        while (keepAsking) {
            System.out.print("\ntriage> ");
            String line = console.nextLine();
            processLine(line, priQueue);
        }

        System.out.println(MSG_GOODBYE);
        
        console.close();
    }

    // enum representing priorities
    private enum Priority {
    	// most urgent
		immediate(1),
		// emergency
		emergency(2),
		// urgent
		urgent(3),
		// least urgent
		minimal(4);
		
		public int getCode() {
			return code;
		}

		private int code;
		
		private Priority(int codeNum) {
			this.code = codeNum;
		}
	}

	/**
     * Process the line entered from the user or read from the file
     * @param line     String command to execute
     * @param priQueue Priority Queue to operate on
     */
    private static void processLine(String line,
                                    PatientPriorityQueue priQueue) {

        Scanner lineScanner = new Scanner(line); // Scanner to extract words
        String cmd = null;     // The first is user's command
        try {
        	cmd = lineScanner.next();
        } catch (NoSuchElementException e) { // user entered nothing; continue
        	lineScanner.close();
        	return;
        }

        // A switch statement could be used on strings, but not all have JDK7
        if (cmd.equals("help")) {
            System.out.println(MSG_HELP);
        } else if (cmd.equals("add")) {
            addPatient(lineScanner, priQueue);
        } else if (cmd.equals("peek")) {
            peekNextPatient(priQueue);
        } else if (cmd.equals("next")) {
            dequeueNextPatient(priQueue);
        } else if (cmd.equals("list")) {
            showPatientList(priQueue);
        } else if (cmd.equals("load")) {
            executeCommandsFromFile(lineScanner, priQueue);
        } else if (cmd.equals("debug")) {
            System.out.println(priQueue.toString());
        } else if (cmd.equals("quit")) {
            keepAsking = false;
        } else {
            System.out.println("Error: unrecognized command: " + line);
        }
    }

    /**
     * Reads a text file with each command on a separate line and executes the
     * lines as if they were typed into the command prompt.
     * @param lineScanner Scanner remaining characters after the command `load`
     * @param priQueue    priority queue to operate on
     */
    private static void executeCommandsFromFile(Scanner lineScanner,
                                                PatientPriorityQueue priQueue) {
        // read the rest of the line into a single string
        String fileName = lineScanner.nextLine().trim();

        try {
            Scanner file = new Scanner(new File(fileName));
            while (file.hasNext()) {
                final String line = file.nextLine();
                System.out.println("\ntriage> " + line);
                processLine(line, priQueue);
            }
            file.close();
        } catch (FileNotFoundException e) {
            System.out.printf("File %s was not found.%n", fileName);
        }
    }

    /**
     * Displays the next patient in the waiting room that will be called.
     * @param priQueue priority queue to operate on
     */
    private static void peekNextPatient(PatientPriorityQueue priQueue) {
    	if (priQueue.size() > 0)
    		System.out.println("Highest priority patient to be called next: "
    				+ priQueue.peek());
    	else
    		System.out.println(NO_PATIENTS_WAITING);
    }

    /**
     * Displays the list of patients in the waiting room.
     * @param priQueue priority queue to operate on
     */
    private static void showPatientList(PatientPriorityQueue priQueue) {
        System.out.println("# patients waiting: " + priQueue.size() + "\n");
        System.out.println("  Arrival #   Priority Code   Patient Name\n" +
                           "+-----------+---------------+--------------+");
        for (Patient p: priQueue.getPatientList())
        	System.out.printf(" %6d       %-14s %-14s%n",
        			p.getArrivalOrder(),
        			getPriority(p.getPriorityCode()),
        			p.getName());
    }
    
    // converts a priority code to its corresponding string representation
	private static String getPriority(int priorityCode) {
    	return EnumSet.allOf(Priority.class)
	    	.stream()
	    	.filter(pc -> pc.getCode() == priorityCode)
	    	.map(String::valueOf)
	    	.findFirst()
	    	.orElseThrow(IllegalArgumentException::new);
    }
    
	// converts a priority string to its corresponding int value
    private static int priorityToCode(String priority) {
    	return Priority.valueOf(priority).getCode();
    }

    /**
     * Removes a patient from the waiting room and displays the name on the
     * screen.
     * @param priQueue priority queue to operate on
     */
    private static void dequeueNextPatient(
        PatientPriorityQueue priQueue) {
    	if (priQueue.size() > 0) {
    		// The next patient in line.
	    	Patient p = priQueue.dequeue();
	    	System.out.println("Next patient: " + p.getName());
    	} else
    		System.out.println(NO_PATIENTS_WAITING);
    }

    /**
     * Adds the patient to the waiting room.
     * @param lineScanner Scanner with remaining chars after the command
     * @param priQueue    priority queue to operate on
     */
    private static void addPatient(Scanner lineScanner,
                                   PatientPriorityQueue priQueue) {
    	String priority = null, // priority from the line
    			patientName = null; // patient name
    	try {
    		priority = lineScanner.next();
    	} catch (NoSuchElementException e) {
    		System.out.println("You must provide a priority.");
    		return;
    	}
    	
    	try {
    		patientName = lineScanner.nextLine();
    	} catch (NoSuchElementException e) {
    		System.out.println("You must provide a patient name.");
    		return;
    	}
    	
    	try {
    		priQueue.addPatient(priorityToCode(priority), patientName);
    		System.out.printf("Added patient \"%s\" to the priority system.%n",
    				patientName);
    	} catch (IllegalArgumentException e) {
    		System.out.println("'" + priority + "' is not a valid "
    				+ "priority!");
    	}
    }
}
