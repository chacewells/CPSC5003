/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package week1.wednesday;

import java.util.Scanner;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Quiz1 {
	public static final double BASE_CASE_VALUE = 12.8;

	public static void main(String[] args) {
		final int SENTINEL = -1;
		int n = 0;
		
		while (n != SENTINEL) {
			Scanner console = new Scanner(System.in);
			System.out.print("n (" + SENTINEL + " to quit)? ");
			n = console.nextInt();
			if (n != SENTINEL) {
				double ans = t(n);
				System.out.println("t(" + n + ") = " + ans);
			}
		}
	}
	
	public static double t(int n) {
		return (n <= 0)
			? BASE_CASE_VALUE
			: 2.0 * t(n - 1) + 1.0 / Math.sqrt(n);
	}

}
