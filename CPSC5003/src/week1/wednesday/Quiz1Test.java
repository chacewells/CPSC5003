/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package week1.wednesday;

import org.junit.Assert;
import org.junit.Test;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Quiz1Test {

	@Test
	public void test() {
		Assert.assertEquals(1.725e31, Quiz1.t(100), 1e28);
	}

}
