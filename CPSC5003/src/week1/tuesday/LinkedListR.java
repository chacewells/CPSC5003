/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package week1.tuesday;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class LinkedListR {
	Node head;
	
	static class Node {
		int value;
		Node next;
		
		Node(int value, Node next) {
			this.value = value;
			this.next = next;
		}
		
		Node(int value) {
			this.value = value;
			this.next = null;
		}
	}
	
	// print linked list recursive
	public void printListR() {
		System.out.println(listToStr(head));
	}
//	
//	private void printListR(Node curr) {
//		if (curr != null) { // stopping case
//			System.out.print(curr.value + " ");
//			printListR(curr.next); // recursive step
//		}
//	}
	
	private String listToStr(Node curr) {
		return (curr != null)
				? curr.value + " " + listToStr(curr.next)
				: "";
	}
	
	public static void main(String[] args) {
		LinkedListR l = new LinkedListR();
		l.head = new Node(1);
		l.head = new Node(5, l.head);
		l.head = new Node(12, l.head);
		
		l.printListR();
	}
}
