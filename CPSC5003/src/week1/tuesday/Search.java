/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package week1.tuesday;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Search {
	
	public static void main(String[] args) {
		int[] a = { 1, 5, 7, 9 };
		int index = linearSearchR(a, 1);
		int index2 = linearSearchR(a, 9);
		
		System.out.println("found 1 at " + index);
		System.out.println("found 9 at " + index2);
		
		int index3 = linearSearchR(a, 10);
		System.out.println("found 10 at " + index3);
	}
	
	public static int linearSearchItr(int[] arr, int key) {
		for (int i = 0; i < arr.length; ++i) {
			if (key == arr[i]) {
				return i;
			}
		}
		
		return -1;
	}
	
	public static int linearSearchR(int[] arr, int key) {
		return linearSearchR(arr, key, 0);
	}
	
	private static int linearSearchR(int[] arr, int key, int left) {
		if (left >= arr.length)
			return -1;
		if (key == arr[left])
			return left;
		
		return linearSearchR(arr, key, left + 1);
	}

}
