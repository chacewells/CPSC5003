/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Lab2<br>
 * Reads input from the user and reports whether the input is a palindrome.
 * 
 * @author Aaron Wells
 * @version 1.0
 *
 */
public class Lab2 {

	/**
	 * See class header.
	 * @param args If args[0] exists, assume it is a file path and use it 
	 * for input.
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		String testText; // the text to test for palindrome
		
		try (BufferedReader console = getConsole(args)) {
			do {
				System.out.print("Test text (ENTER to exit): ");
				testText = console.readLine();
				if (isBlank(testText))
					continue;
				
				System.out.println("This is " +
						(isPalindrome(testText) ? "" : "NOT ") +
						"a palindrome!\n");
			} while (!isBlank(testText));
		}
		
		System.out.println("\nThanks for using the palindrome tester!");
	}
	
	/**
	 * Tests whether the string is a palindrome.<br>
	 * Note: ignores whitespace and capitalization.
	 * 
	 * @param text The text to test.
	 * @return True if the text reads the same backward as it does forward;
	 * false otherwise.
	 */
	public static boolean isPalindrome(String text) {
		char[] preprocessedText = preprocess(text).toCharArray();
		
		return isPalindrome(preprocessedText, 0, preprocessedText.length - 1);
	}
	
	// Tests whether the string is blank
	private static boolean isBlank(String s) {
		return null == s || "".equals(s);
	}
	
	// returns a console from a file or stdin if args is empty
	private static BufferedReader getConsole(String[] args) throws IOException {
		if (args.length > 0 && args[0] != null) {
			return Files.newBufferedReader(Paths.get(args[0]));
		}
		
		return new BufferedReader(new InputStreamReader(System.in));
	}
	
	// removes whitespace from a string
	private static String preprocess(String text) {
		final String whitespaceRegex = "\\s+";
		return text.toLowerCase().replaceAll(whitespaceRegex, "");
	}
	
	// helper method for isPalindrome()
	private static boolean isPalindrome(char[] charText, int first, int last) {
		if (first >= last)
			return true;
		
		return charText[first] == charText[last] // short-circuits here
				&& isPalindrome(charText, first + 1, last - 1);
	}
	
}
