/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab1;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.function.BiFunction;

/**
 * Lab1<br>
 * Demonstrates two implementations of binary search; one iterative; the other
 * recursive. Reads a resource file into memory as an array of integers, sorts
 * the array in ascending order, then verifies the expected search indexes for
 * 3 test cases:
 * <ol>
 * <li>The lowest value in the array</li>
 * <li>The middle value in the array</li>
 * <li>The last value in the array</li>
 * <li>A value that doesn't exist in the array</li>
 * </ol>
 * 
 * @author Aaron Wells
 * @version 1.0
 *
 */
public class Lab1 {

	/**
	 * See class header.
	 * 
	 * @param args No program arguments expected.
	 * @throws IOException If there's an issue reading the file.
	 * @throws URISyntaxException If there's an issue finding the file.
	 */
	public static void main(String[] args)
			throws IOException, URISyntaxException {
		int[] nums = readNumbersFromFile("lab1-test.txt");
		selectionSort(nums);
		System.out.println("Sorted array: " + Arrays.toString(nums));
		System.out.println();

		System.out.println("Iterative binary search:");
		testBinarySearchFn(Lab1::binarySearchItr, nums);
		
		System.out.println();
		System.out.println("Recursive binary search:");
		testBinarySearchFn(Lab1::binarySearchRec, nums);
	}
	
	// Test a binary search function by function reference
	private static void testBinarySearchFn(
			BiFunction<Integer, int[], Integer> binarySearchFn,
			int[] nums) {
		int lowest = -92, lowPos = 0; // lowest value and position
		int middle = 34, midPos = 19; // middle value and position
		int highest = 99, highPos = 36; // highest value and position
		int none = 15, nonePos = -1; // not found value and position
		
		int actualLow = binarySearchFn.apply(lowest, nums);
		System.out.printf("Low - value %d; expected %d; actual %d%n", 
				lowest, lowPos, actualLow);
		
		int actualMid = binarySearchFn.apply(middle, nums);
		System.out.printf("Mid - value %d; expected %d; actual %d%n", 
				middle, midPos, actualMid);
		
		int actualHigh = binarySearchFn.apply(highest, nums);
		System.out.printf("High - value %d; expected %d; actual %d%n", 
				highest, highPos, actualHigh);
		
		int actualNone = binarySearchFn.apply(none, nums);
		System.out.printf("Not found - value %d; expected %d; actual %d%n", 
				none, nonePos, actualNone);
	}
	
	/**
	 * Binary search algorithm, implemented iteratively.
	 * 
	 * @param key The search key.
	 * @param arr The array to search.
	 * @return The index of the key if found; otherwise -1.
	 */
	public static int binarySearchItr(int key, int[] arr) {
		int first = 0;
		int last = arr.length - 1;
		int mid;
		
		while (first <= last) {
			mid = (last + first) / 2;
			
			if (key == arr[mid])
				return mid;
			else if (key < arr[mid])
				last = mid - 1;
			else // key > arr[mid]
				first = mid + 1;
		}
		
		return -1; // not found
	}
	
	/**
	 * Binary search algorithm, implemented recursively.
	 * 
	 * @param key The search key.
	 * @param arr The array to search.
	 * @return The index of the key if found; otherwise -1.
	 */
	public static int binarySearchRec(int key, int[] arr) {
		return binarySearchRec(key, arr, 0, arr.length - 1);
	}
	
	// recursive helper method for binary search
	private static int binarySearchRec(int key, int[] arr, int first,int last) {
		if (first > last)
			return -1;
		
		int mid = (first + last) / 2;
		
		if (key == arr[mid])
			return mid;
		else if (key < arr[mid])
			return binarySearchRec(key, arr, first, mid - 1);
		else // key > arr[mid]
			return binarySearchRec(key, arr, mid + 1, last);
	}
	
	// read integers from file into an array
	private static int[] readNumbersFromFile(String filename)
			throws IOException, URISyntaxException {
		URI uri = Lab1.class.getResource(filename).toURI();
		return Files.newBufferedReader(
				Paths.get(uri))
				.lines()
				.mapToInt(Integer::valueOf)
				.toArray();
	}
	
	// selection sort
	private static void selectionSort(int[] array) {
		for (int left = 0; left < array.length; ++left) {
			int right = findSmallest(array, left, array.length);
			swapElements(array, left, right);
		}
	}
	
	// selection sort helper finds index of smallest element in range
	private static int findSmallest(int[] array, int first, int last) {
		int result = first;
		
		for (int i = first; i < last; ++i) {
			if (array[i] < array[result]) {
				result = i;
			}
		}
		
		return result;
	}
	
	// swap two elements in an array
	private static void swapElements(int[] array, int left, int right) {
		int temp = array[left];
		array[left] = array[right];
		array[right] = temp;
	}

}
