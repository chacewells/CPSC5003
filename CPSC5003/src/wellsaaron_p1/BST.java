/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p1;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Generic binary search tree implementation for Containing Comparable types.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class BST<E extends Comparable<E>> {

	/**
	 * Public constructor initializes an empty binary search tree.
	 */
	public BST() {
		root = null;
		count = 0;
	}
	
	/**
	 * Adds the element to the tree.
	 * @param insertValue The element to add.
	 */
	public void insert(E insertValue) {
		root = insert(insertValue, root);
	}

	/**
	 * Whether the tree contains x.
	 * @param searchValue The element to search.
	 * @return True if x is in the tree; false otherwise.
	 */
	public boolean contains(E searchValue) {
		return contains(searchValue, root);
	}
	
	/**
	 * Removes the element from the tree.
	 * @param removeValue The element to remove.
	 */
	public void remove(E removeValue) {
		root = remove(removeValue, root);
	}
	
	/**
	 * Whether the BST is empty.
	 * 
	 * @return True if size() == 0; false otherwise.
	 */
	public boolean empty() {
		return size() == 0;
	}

	/**
	 * The number of elements in this binary search tree.
	 * 
	 * @return The count of elements.
	 */
	public int size() {
		return count;
	}
	
	/**
	 * The number of leaf nodes in the binary search tree.
	 * @return The number of leaf nodes in the binary search tree.
	 */
	public int getLeafNodeCount() {
		return getLeafNodeCount(root);
	}

	/**
	 * The height of the binary search tree. 0 for empty trees, 1 for single-
	 * node trees, 2 for root trees with 1 generation of child-nodes and so
	 * forth.
	 * 
	 * @return The height of this binary search tree.
	 */
	public int getTreeHeight() {
		return getTreeHeight(root);
	}
	
	/**
	 * Determines the level of the given element.
	 * 
	 * @param target The element for which to determine the level.
	 * @return The level of the element.
	 */
	public int getElementLevel(E target) {
		Node<E> curr = root; // the current node
		int level = 0; // the level
		while (curr != null) {
			++level; // increment for each node traversed
			if (target.compareTo(curr.value) > 0) // greater; search right
				curr = curr.right;
			else if (target.compareTo(curr.value) < 0) // less; searh left
				curr = curr.left;
			else // target found
				return level;
		}
		
		// target not found
		return -1;
	}
	
	/**
	 * A list of elements in pre-order traversal ordering.
	 * 
	 * @return The pre-order traversal elements.
	 */
	public ArrayList<E> getPreOrderTraversal() {
		ArrayList<E> ret = new ArrayList<>(); // the traversal list
		buildPreOrderTraversal(ret, root);
		
		return ret;
	}

	/**
	 * A list of elements in in-order traversal ordering.
	 * 
	 * @return The in-order traversal elements.
	 */
	public ArrayList<E> getInOrderTraversal() {
		ArrayList<E> ret = new ArrayList<>(); // the traversal list
		buildInOrderTraversal(ret, root);
		
		return ret;
	}

	/**
	 * A list of elements in post-order traversal ordering.
	 * 
	 * @return The post-order traversal elements.
	 */
	public ArrayList<E> getPostOrderTraversal() {
		ArrayList<E> ret = new ArrayList<>(); // the traversal list
		buildPostOrderTraversal(ret, root);
		
		return ret;
	}
	
	/**
	 * A list of the ancestor elements to a given element.
	 * 
	 * @param val The element to find.
	 * @return A list of the ancestor elements to a given element.
	 */
	public ArrayList<E> getAncestorsOf(E val) {
		ArrayList<E> ret = new ArrayList<>(); // the list of ancestors
		addAncestorsOf(val, root, ret);
		return ret;
	}
	
	@Override
	public String toString() {
		return new TreePrinter().getStringReprOf(root);
	}
	
	private Node<E> root; // the root tree
	private int count; // number of elements in the binary search tree

	// helper for contains()
	private boolean contains(E target, Node<E> curr) {
		if (curr == null)
			return false;
		if (target.compareTo(curr.value) == 0)
			return true;
	
		return (target.compareTo(curr.value) < 0)
			? contains(target, curr.left)
			// x.compareTo(curr.value) > 0
			: contains(target, curr.right);
	}

	// helper for add()
	private Node<E> insert(E target, Node<E> curr) {
		if (curr == null) {
			count++;
			return new Node<E>(target);
		} if (target.compareTo(curr.value) < 0)
			curr.left = insert(target, curr.left);
		else if (target.compareTo(curr.value) > 0)
			curr.right = insert(target, curr.right);
		else
			// replace; although comparison is a match, incoming value could 
			// use a different representation
			curr.value = target;
		return curr;
	}
	
	// in class implementation
	private Node<E> remove(E target, Node<E> curr) {
		if (curr == null)
			return null; // x is not in the BST
		if (target.compareTo(curr.value) < 0)
			curr.left = remove(target, curr.left);
		else if (target.compareTo(curr.value) > 0)
			curr.right = remove(target, curr.right);
		else {
			// found the node with value to delete
			if (curr.left == null) {
				count--;
				return curr.right; // also works when curr is a leaf
			} else if (curr.right == null) {
				count--;
				return curr.left;
			} else {
				// pull in a suitable replacement and then delete where it came 
				// from
				E replacement = getMax(curr.left);
				curr.value = replacement;
				curr.left = remove(replacement, curr.left);
			}
		}
		return curr;
	}
	
	// get the largest element for a given node
	private E getMax(Node<E> curr) {
		while (curr.right != null)
			curr = curr.right;
		
		return curr.value;
	}
	
	// traverse the tree, counting the number of leaf nodes
    private int getLeafNodeCount(Node<E> curr) {
    	if (curr != null) { // potentially is a leaf
			if (curr.left == null && curr.right == null) // this is a leaf
				return 1;
			else // both children contain leaves; return sum
				return getLeafNodeCount(curr.left) 
						+ getLeafNodeCount(curr.right);
    	}
    	
    	return 0;
	}

    // the tree height
	private int getTreeHeight(Node<E> curr) {
		if (curr != null) {
			if (curr.left == null && curr.right == null)
				return 1;
			else
				return 1 + Math.max(
						getTreeHeight(curr.left),
						getTreeHeight(curr.right));
		}
		
		return 0;
	}

	// helper method for getPreOrderTraversal
	private void buildPreOrderTraversal(ArrayList<E> ret, Node<E> curr) {
		if (curr != null) {
			ret.add(curr.value);
			buildPreOrderTraversal(ret, curr.left);
			buildPreOrderTraversal(ret, curr.right);
		}
	}

	// build list with inorder ordering
	private void buildInOrderTraversal(ArrayList<E> ret, Node<E> curr) {
		if (curr != null) {
			buildInOrderTraversal(ret, curr.left);
			ret.add(curr.value);
			buildInOrderTraversal(ret, curr.right);
		}
	}

	// helper method for getPostOrderTraversal
	private void buildPostOrderTraversal(ArrayList<E> ret, Node<E> curr) {
		if (curr != null) {
			buildPostOrderTraversal(ret, curr.left);
			buildPostOrderTraversal(ret, curr.right);
			ret.add(curr.value);
		}
	}

	// helper method for getAncestorsOf(E)
	private boolean addAncestorsOf(E val, Node<E> curr, ArrayList<E> ret) {
		if (curr != null) {
			int comp = val.compareTo(curr.value);
			if (comp < 0) {
				if (addAncestorsOf(val, curr.left, ret)) {
					ret.add(curr.value);
					return true;
				}
			} if (comp > 0) {
				if (addAncestorsOf(val, curr.right, ret)) {
					ret.add(curr.value);
					return true;
				}
			} else
				return true;
		}
		
		return false;
	}

	// a tree node
	private static class Node<E> {
        private E value; // the element value
        private Node<E> left; // left child
        private Node<E> right; // right child

        private Node(E value) {
            this.value = value;
            this.left = null;
            this.right = null;
        }
    }
	
    /**
     * Helper to aid in printing the tree.
     */
    private class TreePrinter {

        /**
         * Generates the string representation of the given tree rooted at the
         * node
         * @param node tree to represent
         * @return String representation of the given tree rooted at the node
         */
        private String getStringReprOf(Node<E> node) {
            if (node == null) {
                return "(empty)";
            }

            Block blk = getBlock(node);
            StringBuilder sb = new StringBuilder();
            for (StringBuilder line : blk.lines) {
                sb.append(line).append('\n');
            }
            return sb.toString();
        }

        private Block getBlock(Node<E> node) {
            // min spacing between left and right blocks
            final int SP = 2;

            // base case
            if (node == null) {
                return null;
            }

            Block lft = getBlock(node.left);
            Block rgt = getBlock(node.right);
            boolean hasLft = lft != null;
            boolean hasRgt = rgt != null;

            // root value and root length
            String val = node.value.toString();
            int len = node.value.toString().length();

            // how much the right block needs to be shifted and the width of all
            int rgtShift = hasLft ? (lft.width + SP) : 0;
            int width = rgtShift + (hasRgt ? rgt.width : 0);

            // where should the root attach if there is left blk?
            int rootIdx = hasLft ? lft.toIdx + 1 : 0;

            // where should the root be positioned if also have right blk?
            if (hasRgt) {
                int rgtRootAttachIdx = rgt.fmIdx + rgtShift - 1 - len;
                if (rgtRootAttachIdx < rootIdx) {
                    // the right block needs to move right more
                    int moreShift = rootIdx - rgtRootAttachIdx;
                    rgtShift += moreShift;
                    width += moreShift;
                } else {
                    // the root needs to be positioned in-between
                    rootIdx = rootIdx + (rgtRootAttachIdx - rootIdx) / 2;
                }
            } else {
                width = Math.max(width, rootIdx + len);
            }

            // build the line with the root
            StringBuilder line = new StringBuilder();
            padUntil(line, ' ', rootIdx);
            line.append(val);
            padUntil(line, ' ', width);

            // start building a new block
            Block result = new Block();
            result.lines.add(line);

            // build the line with the leads if have children
            if (hasLft || hasRgt) {
                line = new StringBuilder();
                if (hasLft) {
                    padUntil(line, ' ', lft.toIdx);
                    padUntil(line, '_', rootIdx - 1);
                    line.append('/');
                }
                if (hasRgt) {
                    padUntil(line, ' ', rootIdx + len);
                    line.append('\\');
                    padUntil(line, '_', rgt.fmIdx + rgtShift);
                }
                padUntil(line, ' ', width);
                result.lines.add(line);
            }

            // add combined children lines
            result.lines.addAll(combinedLines(lft, rgt, rgtShift));
            result.width = width;
            result.fmIdx = rootIdx;
            result.toIdx = rootIdx + len;
            return result;
        }

        /**
         * Combines the children blocks with the given shift
         * @param lft Left Block to combine
         * @param rgt Right Block to combine
         * @param shift amount to shift the right block
         * @return combined lines
         */
        private ArrayList<StringBuilder> combinedLines(Block lft, Block rgt,
                                                       int shift) {
            ArrayList<StringBuilder> lines = new ArrayList<StringBuilder>();
            if (lft == null) {
                if (rgt != null) {
                    for (StringBuilder sb : rgt.lines) {
                        StringBuilder line = new StringBuilder();
                        padSpUntil(line, shift);
                        line.append(sb);
                        lines.add(line);
                    }
                }
                return lines;
            } else if (rgt == null) {
                return lft.lines;
            }

            final Iterator<StringBuilder> lftIt = lft.lines.iterator();
            final Iterator<StringBuilder> rgtIt = rgt.lines.iterator();

            while (lftIt.hasNext() || rgtIt.hasNext()) {
                StringBuilder sb =
                    lftIt.hasNext() ? lftIt.next() : new StringBuilder();
                padSpUntil(sb, shift);
                if (rgtIt.hasNext()) {
                    sb.append(rgtIt.next());
                }
                lines.add(sb);
            }
            return lines;
        }

        /**
         * Helper to add multiple characters to the StringBuilder
         * @param sb  StringBuilder to append to
         * @param c   character to add
         * @param len add until sb is this length
         */
        private void padUntil(StringBuilder sb, char c, int len) {
            while (sb.length() < len) {
                sb.append(c);
            }
        }

        /**
         * Helper to add multiple characters to the StringBuilder
         * @param sb  StringBuilder to pad
         * @param len add until sb is this length
         */
        private void padSpUntil(StringBuilder sb, int len) {
            while (sb.length() < len) {
                sb.append(' ');
            }
        }

        /**
         * Print Block
         */
        private class Block {
            // String lines
            ArrayList<StringBuilder> lines = new ArrayList<StringBuilder>();
            int fmIdx = 0;  // index of the root value start on 1st line
            int toIdx = 0;  // index of the root value end on 1st line
            int width = 0;  // how wide the lines are
        }
    }

}
