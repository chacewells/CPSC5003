/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_p1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import com.sun.org.apache.xerces.internal.impl.dv.ValidatedInfo;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;

/**
 * Runs a series of tests against BST to ensure it implements proper
 * BST functionality.
 * 
 * @author Aaron Wells
 *
 */
public class P1 {
	
	/**
	 * See class javadoc.
	 * @param args No command-line arguments expected.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		runTestSuiteWithUser();
	}
	
	// runs the test suite against a variety of data sets to ensure proper
	// functionality. For my own tests; not turned into Canvas
	private static void runTestSuiteHeadless() {
		String[] testFiles = { // the test files
				"p1-pos-less-100.txt",
				"p1-pos-neg-50.txt",
				"p1-wide-range.txt",
				"p1-test.txt"
		};
		List<Integer> testData = null; // the test data
		BST<Integer> tree = null; // the tree to test
		
		for (String filename : testFiles) {
			try {
				testData = loadTestDataFromFile(filename);
			} catch (IOException e) {
				System.err.println("BAD FILE: " + filename);
				e.printStackTrace();
				System.exit(1);
			}
			
			tree = testCreateTree();
			runTestSuite(testData, tree);
		}
	}
	
	// Asks the user for a filename, then runs the test suite against data from
	// the specified file.
	private static void runTestSuiteWithUser() {
		greetUser();
		BST<Integer> tree = testCreateTree();
		List<Integer> testData = getTestDataFromUserSpecifiedFile();
		
		runTestSuite(testData, tree);
	}
	
	/*
	 * the test suite; tests for
	 * insert
	 * traversals
	 * containstests
	 * remove
	 * getElementLevel
	 * getAncestorsOf
	 */
	private static void runTestSuite(List<Integer> testData, BST<Integer> tree)
	{
		testInsert(tree, testData);
		testTraversals(tree);
		testContains(tree);
		testRemove(tree);
		testGetElementLevel(tree);
		testGetAncestorsOf(tree);
	}
	
	// demonstrates proper functionality of getAncestorsOf()
	private static void testGetAncestorsOf(BST<Integer> tree) {
		System.out.println("\nTESTING getAncestorsOf:");
		for (Integer target : getPostOrderSample(tree)) {
			System.out.printf("getAncestorsOf(%d): ",
					target);
			displayList(tree.getAncestorsOf(target));
		}
	}

	// demonstrates proper functionality of getElementLevel()
	private static void testGetElementLevel(BST<Integer> tree) {
		System.out.println("\nTESTING getElementLevel:");
		for (Integer target : getPostOrderSample(tree))
			System.out.printf("getElementLevel(%d): %d%n",
					target,
					tree.getElementLevel(target));
	}
	
	// selects a few nodes at various levels
	private static List<Integer> getPostOrderSample(BST<Integer> tree) {
		ArrayList<Integer> postorder = tree.getPostOrderTraversal();
		Integer first = postorder.get(0);
		Integer mid = postorder.get(postorder.size() / 2);
		Integer last = postorder.get(postorder.size() - 1);
		Integer notIn = randomValuesNotIn(postorder, 1).get(0);
		
		return Arrays.asList(first, mid, last, notIn);
	}
	
	// demonstrates remove functionality
	private static void testRemove(BST<Integer> tree) {
		System.out.print("\nTESTING REMOVE: removing some values: ");
		
		int howMany = tree.size() / 3;
		ArrayList<Integer> inorder = tree.getInOrderTraversal();
		
		List<Integer> removals = new ArrayList<>();
		removals.addAll(randomValuesIn(inorder, howMany));
		removals.addAll(randomValuesNotIn(inorder, howMany));
		
		displayList(removals);
		
		removals.forEach(tree::remove);
		System.out.println("\nTree after removals:");
		displayTreeInfo(tree);
		
		System.out.print("\nAdding back some values: ");
		displayList(removals);
		
		removals.forEach(tree::insert);
		System.out.println("\nTree after adding back some nodes:");
		displayTreeInfo(tree);
	}

	// demonstrates contains() functionality
	private static void testContains(BST<Integer> tree) {
		System.out.println("TESTING CONTAINS");
		int howMany = tree.size() / 3;
		
		ArrayList<Integer> inorder = tree.getInOrderTraversal();
		List<Integer> shouldContain = randomValuesIn(inorder, howMany);
		
		System.out.println("Should contain these..");
		for (Integer value : shouldContain)
			System.out.printf("contains(%d): %s%n",
					value, 
					tree.contains(value));
		
		System.out.println("\nShould NOT contain these..");
		List<Integer> shouldNotContain = randomValuesNotIn(inorder, howMany);
		for (Integer value : shouldNotContain)
			System.out.printf("contains(%d): %s%n",
					value, 
					tree.contains(value));
	}
	
	// retrieves a random set of values from the list
	private static List<Integer> randomValuesIn(
			ArrayList<Integer> values, 
			int howMany) {
		Set<Integer> randSet = new HashSet<>();
		Random rand = new Random();
		int size = values.size();
		
		for (int i = 0; i < howMany; ++i) {
			int randomIndex = rand.nextInt(size);
			randSet.add(values.get(randomIndex));
		}
		
		return randSet.stream().sorted().collect(Collectors.toList());
	}
	
	// retrieves a random set of values, ensuring they are not in the given list
	private static List<Integer> randomValuesNotIn(
			ArrayList<Integer> values,
			int howMany) {
		Set<Integer> valuesSet = new HashSet<>(values);
		Set<Integer> randSet = new HashSet<>();
		Random rand = new Random();

		Integer max = Collections.max(values);
		
		for (int i = 0; i < howMany; ++i) {
			int randomValue = rand.nextInt(max);
			if (valuesSet.contains(randomValue)) {
				--i;
				continue;
			}
			
			randSet.add(randomValue);
		}
		
		return randSet.stream().sorted().collect(Collectors.toList());
	}
	
	// demonstrates traversal functionality: preorder, inorder, postorder
	private static void testTraversals(BST<Integer> tree) {
		System.out.println("TESTING TRAVERSALS");
		
		System.out.println("\nPre-order traversal:");
		displayList(tree.getPreOrderTraversal());
		
		System.out.println("\nIn-order traversal:");
		displayList(tree.getInOrderTraversal());
		
		System.out.println("\nPost-order traversal:");
		displayList(tree.getPostOrderTraversal());
		
		System.out.println();
	}
	
	/*
	 * console.close() not necessary with auto-closeable in try w/ resources
	 * See https://tinyurl.com/nggbv2b
	 */
	private static List<Integer> getTestDataFromUserSpecifiedFile() {
		List<Integer> ret;
		
		try (
				BufferedReader console = new BufferedReader(
						new InputStreamReader(System.in));
		){
			System.out.print("Load file(Enter blank to use predefined nums: ");
			String filenameFromUser = console.readLine();
			ret = isBlank(filenameFromUser)
				? fallbackTestData()
				: loadTestDataFromFile(filenameFromUser);
		} catch (IOException e) {
			ret = fallbackTestData();
		}
		
		return ret;
	}
	
	// the file contents as a list of integers
	private static List<Integer> loadTestDataFromFile(String filename) 
		throws IOException {
		return Files.newBufferedReader(Paths.get(filename))
				.lines()
				.map(Integer::new)
				.collect(Collectors.toList());
	}
	
	// string utility tests for blankness
	private static boolean isBlank(String s) {
		return s == null || s.isEmpty();
	}
	
	// builds a list of default test data
	private static List<Integer> fallbackTestData() {
		return Arrays.asList(
			40,
			20,
			10,
			30,
			60,
			50,
			70
		);
	}
	
	// greets the user (duh)
	private static void greetUser() {
		System.out.println("Welcome to the BST test program.\n");
	}
	
	// creates a tree to test data with
	private static BST<Integer> testCreateTree() {
		System.out.println("\nCreating a tree");
		BST<Integer> tree = new BST<>(); // the bst
		
		System.out.println("Tree is empty: " + tree.empty() + "\n");
		
		return tree;
	}
	
	// demonstrates insert() functionality
	private static void testInsert(
			BST<Integer> tree, 
			List<Integer> valuesToInsert) {
		System.out.println("TESTING INSERT: inserting elements in this order:");
		displayList(valuesToInsert);
		
		valuesToInsert.forEach(tree::insert);
		
		System.out.println("Tree after inserting:");
		displayTreeInfo(tree);
	}
	
	// displays the state of the tree
	private static void displayTreeInfo(BST<Integer> tree) {
		StringBuilder info = new StringBuilder();
		info.append(
				String.format("Tree height: %d%n", tree.getTreeHeight()));
		info.append(
				String.format("# Elements: %d%n", tree.size()));
		info.append(
				String.format("# Leaves: %d%n", tree.getLeafNodeCount()));
		info.append(tree + "\n");
		
		System.out.println(info.toString());
	}
	
	// displays the list elements as space-separated values or indicates whether
	// the list is empty
	private static void displayList(List<?> values) {
		final String delimiter = " ";
		
		System.out.println(
			values.isEmpty()
			? "(empty)"
			: values
				.stream()
				.map(String::valueOf)
				.collect(Collectors.joining(delimiter))
		);
	}
	
}
