/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package ice7_population_density;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Ice7 {
	
	public static void main(String[] args) throws IOException {
		CsvFile csv = new CsvFile("LND01.csv");
		HashMap<Integer,PopDens> data = new HashMap<>();
		
		while (!csv.eof()) {
			HashMap<String, String> row = csv.next();
			int countyId = Integer.parseInt(row.get("STCOU"));
			String name = row.get("Areaname");
			double area = Double.parseDouble(row.get("LND110210D"));
			PopDens pd = new PopDens(name, area);
			data.put(countyId, pd);
		}
		csv.close();
		
		csv = new CsvFile("DEC_10_SF2_PCT1_with_ann.csv");
		csv.next();
		while (!csv.eof()) {
			HashMap<String,String> row = csv.next();
			if ("001".equals(row.get("POPGROUP.id"))) {
				int countyId = Integer.parseInt(row.get("GEO.id2"));
				int population = Integer.parseInt(row.get("D001"));
				data.get(countyId).population = population;
			}
		}
		
		// check King County
		System.out.println(data.get(53033));
		
		ArrayList<PopDens> list = new ArrayList<>();
		for (int countyId: data.keySet()) {
			PopDens pd = data.get(countyId);
			if (pd.population > 0)
				list.add(pd);
		}
		
		list.sort(null);
		
		System.out.println("\nTop 25");
		for (int i = 0; i < 25; ++i)
			System.out.println(i + ": " + list.get(i));
		
		System.out.println("\nBottom 25");
		for (int i = list.size() - 25; i < list.size(); ++i)
			System.out.println(i + ": " + list.get(i));
	}

	private static class PopDens implements Comparable<PopDens> {
		public String name;
		public double area;
		public int population;
		
		public double density() {
			return population / area;
		}
		
		public PopDens(String name, double area) {
			this.name = name;
			this.area = area;
		}

		
		@Override
		public int compareTo(PopDens o) {
			double myDens = density();
			double yourDens = o.density();
			
			if (myDens < yourDens)
				return 1;
			else if (myDens > yourDens)
				return -1;
			else return 0;
		}
		
		@Override
		public String toString() {
			return String.format(
					"%s: %.3f", name, density());
		}
	}
}
