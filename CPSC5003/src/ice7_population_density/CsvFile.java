package ice7_population_density;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class CsvFile {
	public CsvFile(String filename) throws IOException {
      reader = new BufferedReader(new FileReader(filename));
      
      // get first line for column names
      columns = reader.readLine().split(",");
      
      // get rid of beginning of file marks
      int i = 0;
      while (!ok(columns[0].charAt(i)))
      	i++;
      if (i > 0)
      	columns[0] = columns[0].substring(i);
	}
	
	private boolean ok(char c) {
		return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
	}
	
	public void close() throws IOException {
		reader.close();
	}
	
	public boolean eof() throws IOException {
		return !reader.ready();
	}
	
	public HashMap<String, String> next() throws IOException {
		HashMap<String, String> row = new HashMap<>();
		String[] fields = reader.readLine().split(",");
		for (int i = 0, j = 0; j < fields.length && i < columns.length; i++, j++) {
			String field = fields[j];
			if (field.startsWith("\"")) {
				field = field.substring(1);
				String nfield = "";
				for (nfield = fields[++j]; !nfield.endsWith("\""); nfield = fields[++j])
					field += "," + nfield;
				field += "," + nfield.substring(0, nfield.length()-1);
			}
			row.put(columns[i], field);
		}
		return row;
	}
	
	
	
	private BufferedReader reader;
	private String[] columns;
}
