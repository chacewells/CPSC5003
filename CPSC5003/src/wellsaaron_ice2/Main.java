/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_ice2;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.IntStream;

/**
 * Reads integers from a file adds them to a BST, then demonstrates tree
 * traversal and element removal.
 * 
 * @author Aaron Wells
 *
 */
public class Main {
	public static String TEST_FILENAME = "bst-test.dat";

	/**
	 * See class header
	 * @param args No command-line arguments expected.
	 * @throws IOException If the file can't be opened or read.
	 * @throws URISyntaxException If the file can't be found.
	 */
	public static void main(String[] args) 
			throws IOException, URISyntaxException {
		BST tree = new BST();
		slurpDatFile().forEach(tree::add);
		
		preorder(tree);
		inorder(tree);
		postorder(tree);
		
		System.out.println("Before removing elements: ");
		contains(tree, -46);
		contains(tree, -20);
		contains(tree, 28);
		
		System.out.println("Removing -46, -20, 28");
		tree.remove(-46);
		tree.remove(-20);
		tree.remove(28);
		contains(tree, -46);
		contains(tree, -20);
		contains(tree, 28);
		
		slurpDatFile().forEach(x -> {
			tree.remove(x);
			inorder(tree);
		});
	}
	
	// prints tree in preorder
	private static void preorder(BST tree) {
		System.out.print("preorder: ");
		tree.preorder();
	}
	
	// prints tree in inorder
	private static void inorder(BST tree) {
		System.out.print("inorder: ");
		tree.inorder();
	}
	
	// prints tree in postorder
	private static void postorder(BST tree) {
		System.out.print("postorder: ");
		tree.postorder();
	}
	
	// returns the contents of TEST_FILENAME as an integer stream
	private static IntStream slurpDatFile() 
			throws IOException, URISyntaxException {
		URI testFile = Main.class.getResource(TEST_FILENAME).toURI();
		return Files.newBufferedReader(Paths.get(testFile))
				.lines()
				.mapToInt(Integer::parseInt);
	}
	
	// displays whether the tree contains x
	private static void contains(BST tree, int x) {
		System.out.printf("tree.contains(%d): %s%n", x, tree.contains(x));
	}
	
}
