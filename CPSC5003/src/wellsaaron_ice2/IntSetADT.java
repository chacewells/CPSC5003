/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_ice2;

import java.util.ArrayList;

/**
 * ADT Interface for Integer Set.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public interface IntSetADT {
	public boolean isEmpty();
	public int size();
	public boolean contains(int element);
	public void add(int element);
	public void remove(int element);
	public ArrayList<Integer> elements();
}
