/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_ice2;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class IntSetADTTest {
	private IntSetADT bst;

	@Before
	public void setUp() throws Exception {
		bst = new BST();
		bst.add(13);
		bst.add(-6);
		Map<Integer,Integer> foo = new HashMap<>();
	}

	@Test
	public void testContains() {
		Assert.assertTrue(bst.contains(13));
		Assert.assertTrue(bst.contains(-6));
		Assert.assertFalse(bst.contains(1));
	}

}
