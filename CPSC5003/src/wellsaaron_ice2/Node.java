/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_ice2;

/**
 * Binary Search Tree Node
 *
 * @author  Aaron Wells
 * @version 1.0
 */
class Node {
	// the value
	int value;
	// left and right child references
	Node left, right;
	// constructs a new node from value, left and right
	Node(int value, Node left, Node right) {
		this.value = value;
		this.left = left;
		this.right = right;
	}
	// constructs a new node from value, with left and right null
	Node(int value) {
		this(value, null, null);
	}
}