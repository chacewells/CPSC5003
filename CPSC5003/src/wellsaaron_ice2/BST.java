/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_ice2;

import java.util.ArrayList;

/**
 * Binary Search Tree
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class BST implements IntSetADT {
	
	public BST() {
		root = null;
		count = 0;
	}
	
	/**
	 * Whether the tree contains x.
	 * @param x The element to search.
	 * @return True if x is in the tree; false otherwise.
	 */
	public boolean contains(int x) {
		return contains(x, root);
	}
	
	/**
	 * Adds the element to the tree.
	 * @param x The element to add.
	 */
	public void add(int x) {
		root = add(x, root);
	}
	
	/**
	 * Removes the element from the tree.
	 * @param x The element to remove.
	 */
	public void remove(int x) {
		root = remove(x, root);
	}
	
	/**
	 * Traverses the tree in inorder fashion, printing each node.
	 */
	public void inorder() {
		inorder(root);
		System.out.println();
	}
	
	/**
	 * Traverses the tree in preorder fashion, printing each node.
	 */
	public void preorder() {
		preorder(root);
		System.out.println();
	}

	/**
	 * Traverses the tree in postorder fashion, printing each node.
	 */
	public void postorder() {
		postorder(root);
		System.out.println();
	}
	
	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public int size() {
		return count;
	}

	@Override
	public ArrayList<Integer> elements() {
		ArrayList<Integer> ret = new ArrayList<>();
		elements(ret, root);
		
		return ret;
	}
	
	private Node root; // the root tree
	private int count; // number of elements in the binary search tree

	// helper for contains()
	private boolean contains(int x, Node curr) {
		if (curr == null)
			return false;
		if (x == curr.value)
			return true;
	
		return (x < curr.value)
			? contains(x, curr.left)
			// x > curr.value
			: contains(x, curr.right);
	}

	// helper for add()
	private Node add(int x, Node curr) {
		if (curr == null) {
			count++;
			return new Node(x);
		} if (x < curr.value)
			curr.left = add(x, curr.left);
		else if (x > curr.value)
			curr.right = add(x, curr.right);
		// else do nothing -- already in set
		return curr;
	}
	
	// in class implementation
	private Node remove(int x, Node curr) {
		if (curr == null)
			return null; // x is not in the BST
		if (x < curr.value)
			curr.left = remove(x, curr.left);
		else if (x > curr.value)
			curr.right = remove(x, curr.right);
		else {
			// found the node with value to delete
			if (curr.left == null) {
				count--;
				return curr.right; // also works when curr is a leaf
			} else if (curr.right == null) {
				count--;
				return curr.left;
			} else {
				// pull in a suitable replacement and then delete where it came from
				int replacement = getMax(curr.left);
				curr.value = replacement;
				curr.left = remove(replacement, curr.left);
			}
		}
		return curr;
	}
	
	// 
	private int getMax(Node curr) {
		while (curr.right != null)
			curr = curr.right;
		
		return curr.value;
	}

	/*
	// helper for remove()
	private Node remove(int x, Node curr) {
		if (curr != null) {
			if (x == curr.value)
				curr = removeNode(curr);
			else if (x < curr.value)
				curr.left = remove(x, curr.left);
			else
				curr.right = remove(x, curr.right);
		}
		
		return curr;
	}

	// precondition: curr is not null
	// helper for removeNode()
	private Node removeNode(Node curr) {
		Node newCurr = null; // new node to replace curr
		if (curr.left != null) {
			newCurr = curr.left;
			newCurr.right = curr.right;
		} else if (curr.right != null) {
			newCurr = curr.right;
			newCurr.left = curr.left;
		}
		
		return newCurr;
	}
	*/

	// helper for inorder()
	private void inorder(Node curr) {
		if (curr != null) {
			inorder(curr.left);
			System.out.print(curr.value + " ");
			inorder(curr.right);
		}
	}

	// helper for preorder()
	private void preorder(Node curr) {
		if (curr != null) {
			System.out.print(curr.value + " ");
			preorder(curr.left);
			preorder(curr.right);
		}
	}

	// helper for postorder()
	private void postorder(Node curr) {
		if (curr != null) {
			postorder(curr.left);
			postorder(curr.right);
			System.out.print(curr.value + " ");
		}
	}

	// build list with inorder ordering
	private void elements(ArrayList<Integer> ret, Node curr) {
		if (curr != null) {
			elements(ret, curr.left);
			ret.add(curr.value);
			elements(ret, curr.right);
		}
	}

}
