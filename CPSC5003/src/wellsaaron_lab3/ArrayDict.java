/*
 * Kevin Lundeen
 * CPSC 5003, Seattle University
 * Summer 2018
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab3;

import java.util.ArrayList;

public class ArrayDict implements DictionaryADT {

	public ArrayDict() {
		members = new ArrayList<>();
	}

	public boolean contains(String key) {
		for (KeyValuePair kv : members)
			if (kv.key.equals(key))
				return true;
		return false;
	}

	public String get(String key) {
		for (KeyValuePair kv : members)
			if (kv.key.equals(key))
				return kv.value;
		return null;
	}

	public void add(String key, String value) {
		for (KeyValuePair kv : members)
			if (kv.key.equals(key)) {
				kv.value = value;
				return;
			}
		members.add(new KeyValuePair(key, value));
	}

	public void remove(String key) {
		for (int i = 0; i < members.size(); i++)
			if (members.get(i).key.equals(key)) {
				members.remove(i);
				return;
			}
		// not found -- do nothing
	}

	public boolean isEmpty() {
		return members.isEmpty();
	}

	public int size() {
		return members.size();
	}

	public ArrayList<String> keys() {
		ArrayList<String> ret = new ArrayList<>();
		for (KeyValuePair kv : members)
			ret.add(kv.key);
		return ret;
	}

	private ArrayList<KeyValuePair> members;

	private class KeyValuePair {
		public String key;
		public String value;

		public KeyValuePair(String key, String value) {
			this.key = key;
			this.value = value;
		}
	}
}
