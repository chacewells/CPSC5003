/*
 * Aaron Wells
 * CPSC 5003, Seattle University
 * Summer 2018
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab3;

import java.util.ArrayList;

public class BSTDict implements DictionaryADT {
	/**
	 * Allocates an empty BSTDict.
	 */
   public BSTDict() {
      root = null;
      count = 0;
   }
   
   @Override
   public boolean contains(String key) {
   		return contains(key, root);
   }
   
   private boolean contains(String key, Node curr) {
		if (curr == null)
			return false;
		else if (key.compareTo(curr.key) < 0)
			return contains(key, curr.left);  // Search the left branch
		else if (key.compareTo(curr.key) > 0)
			return contains(key, curr.right); // Search the right branch
		else // if (key.compareTo(bstree.value) == 0)
			return true;
	}
   
   @Override
   public String get(String key) {
   		return get(key, root);
   }
   
	private String get(String key, Node curr) {
		if (curr == null)
			return null;
		else if (key.compareTo(curr.key) < 0)
			return get(key, curr.left);  // Search the left branch
		else if (key.compareTo(curr.key) > 0)
			return get(key, curr.right); // Search the right branch
		else // if (key.compareTo(bstree.value) == 0)
			return curr.value;
	}
   
	@Override
	public void add(String key, String value) {
		root = add(key, value, root);
	}
	
	private Node add(String key, String value, Node curr) {
		if (curr == null) {
			count++;
			return new Node(key, value);
		}
		if (key.compareTo(curr.key) < 0)
			curr.left = add(key, value, curr.left);
		else if (key.compareTo(curr.key) > 0)
			curr.right = add(key, value, curr.right);
		return curr;
	}
	
	public void inorder() {
		inorder(root);	
	}
	
	private void inorder(Node curr) {
		if (curr != null) {
			inorder(curr.left);
			System.out.printf("%s:%s ", curr.key, curr.value);
			inorder(curr.right);
		}
	}
	
	public void preorder() {
		preorder(root);	
	}
	
	private void preorder(Node curr) {
		if (curr != null) {
			System.out.printf("%s:%s ", curr.key, curr.value);
			preorder(curr.left);
			preorder(curr.right);
		}
	}
	
	public void postorder() {
		postorder(root);
	}

	private void postorder(Node curr) {
		if (curr != null) {
			postorder(curr.left);
			postorder(curr.right);
			System.out.printf("%s:%s ", curr.key, curr.value);
		}
	}
	
	@Override
	public void remove(String key) {
		root = remove(key, root);
	}
	
	private Node remove(String x, Node curr) {
		if (curr == null)
			return null;
		if (x.compareTo(curr.key) < 0) {
			curr.left = remove(x, curr.left);
		} else if (x.compareTo(curr.key) > 0) {
			curr.right = remove(x, curr.right);
		} else {
			// remove this one
			if (curr.left == null) {
				count--;
				return curr.right;  // this branch also if curr is a leaf
			} else if (curr.right == null) {
				count--;
				return curr.left;
			} else {
				// has both right and left, so we need to swap with predecessor node
				Node max = getMax(curr.left);
				curr.key = max.key;
				curr.value = max.value;
				// and then remove the node that had the predecessor
				curr.left = remove(curr.key, curr.left);
			}
		}
		return curr;
	}
	
	private Node getMax(Node curr) {
		while (curr.right != null)
			curr = curr.right;
		return curr;
	}
   
   private Node root;
   private int count;

   private class Node {
	  String key;
      String value;
      Node left; 
      Node right;

      Node(String key, String value, Node left, Node right) {
    	 this.key = key;
         this.value = value;
         this.left = left;
         this.right = right;
      }
          
      Node(String key, String value) {
   			this(key, value, null, null);
      }
   }

   @Override
	public boolean isEmpty() {
		return count == 0;
	}

	@Override
	public int size() {
		return count;
	}

	public ArrayList<String> keys() {
		ArrayList<String> ret = new ArrayList<>();
		elementsInOrder(ret, root);
		return ret;
	}
	
	private void elementsInOrder(ArrayList<String> ret, Node curr) {
		if (curr == null)
			return;
		elementsInOrder(ret, curr.left);
		ret.add(curr.key);
		elementsInOrder(ret, curr.right);
	}
	
	/**
	 * Informal test for BSTDict
	 * 
	 * @param args No command-line args expected.
	 */
	public static void main(String[] args) {
		BSTDict tree = new BSTDict();
		String[] keys = { "bat", "fox", "ant", "cat", "yak", "dog", "elk", "poodle" };

		System.out.println("adding animals (keys lowercase, values uppercase)");
		for (String key : keys) {
			String value = key.substring(0, 1).toUpperCase() + key.substring(1);
			tree.add(key, value);
		}
		
		for (String key : keys)
			System.out.println("Value for " + key + " is: " + tree.get(key));

		System.out.print("\ninorder: ");
		tree.inorder();

		tree.remove("elk");
		System.out.println("\n\nremoving animals");
		for (String key : keys) {
			tree.remove(key);
			System.out.print("\ninorder: ");
			tree.inorder();
		}
	}
}
