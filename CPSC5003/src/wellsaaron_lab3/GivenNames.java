package wellsaaron_lab3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class GivenNames {
	private static final String SOURCE = "propernames.txt";
	private static Random rand;
	private static ArrayList<String> names;
	
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++)
			System.out.println(getRandom());
	}
	
	public static String getRandom() {
		try {
			load();
		} catch (IOException e) {
			names.add("George");
			names.add("Martha");
			names.add("Bob");
		}
		return names.get(rand.nextInt(names.size()));
	}
	
	private static void load() throws IOException {
		if (names != null)
			return;
		rand = new Random();
		names = new ArrayList<>();
		File file = new File(SOURCE);
		FileReader reader = new FileReader(file);
		BufferedReader breader = new BufferedReader(reader);
		String line;
		while ((line = breader.readLine()) != null)
			names.add(line);
		reader.close();
	}
}
