/*
 * Aaron Wells
 * CPSC 5003, Seattle University
 * Summer 2018
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_lab3;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests two implementations of the DictionaryADT class: BSTDict and ArrayDict.
 * In some cases, we are only able to determine that they both behave
 * identically. In other cases, we can also determine correctness as 
 * well.
 * <br>
 * <br>
 * Note to TA, propernames.txt will need to be in the project root in order for
 * GivenNames to get a nice random sampling of names.
 * 
 * @author Aaron Wells
 * @version 1.0
 */
public class DictionaryADTTest {
	private static final int SIZE = 100; // size of each dictionary
	private DictionaryADT bst; // the ADT to test
	private DictionaryADT arr; // the reference ADT
	private Random rand; // Helper to pick out random data to test.

	/**
	 * Sets up a set of each type and populates with SIZE random
	 * elements. (Resulting size is expected to be less than SIZE
	 * since there will likely be some elements chosen randomly more
	 * than once.)
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		bst = new BSTDict();
		arr = new ArrayDict();
		rand = new Random();
		
		Assert.assertTrue(bst.isEmpty());
		Assert.assertTrue(arr.isEmpty());
		for (int i = 0; i < SIZE; i++) {
			String n = GivenNames.getRandom();
			bst.add(n.toLowerCase(), n);
			arr.add(n.toLowerCase(), n);
		}
	}

	/**
	 * Tests that both sets contain the elements of
	 * the other.
	 */
	@Test
	public void testContains() {
		for (String n : bst.keys())
			Assert.assertTrue(arr.contains(n));
		for (String n : arr.keys())
			Assert.assertTrue(bst.contains(n));
	}

	/**
	 * Tests that random removals result in equivalent
	 * smaller sets.
	 */
	@Test
	public void testRemove() {
		int sz = bst.size(); // initial BST size
		Assert.assertEquals(sz, arr.size());
		ArrayList<String> elements = bst.keys();
		for (int i = 0; i < SIZE / 10; i++) {
			String n = elements.get(rand.nextInt(elements.size()));
			bst.remove(n);
			arr.remove(n);
		}
		Assert.assertTrue(sz > bst.size());
		Assert.assertEquals(bst.size(), arr.size());
		for (String n : bst.keys())
			Assert.assertTrue(arr.contains(n));
		for (String n : arr.keys())
			Assert.assertTrue(bst.contains(n));
	}

	/**
	 * Tests that size and isEmpty methods work as expected
	 * before and after removal of all elements.
	 */
	@Test
	public void testSize() {
		Assert.assertFalse(bst.isEmpty());
		Assert.assertFalse(arr.isEmpty());
		int sz = bst.size(); // initial BST size
		Assert.assertTrue(sz <= SIZE);
		Assert.assertEquals(sz, arr.size());
		for (String n : arr.keys()) {
			bst.remove(n);
			arr.remove(n);
		}
		Assert.assertEquals(0, bst.size());
		Assert.assertEquals(0, arr.size());
		Assert.assertTrue(bst.isEmpty());
		Assert.assertTrue(arr.isEmpty());
	}

}
