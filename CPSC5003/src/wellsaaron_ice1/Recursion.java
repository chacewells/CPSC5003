/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package wellsaaron_ice1;

import static java.lang.Math.min;

/**
 * Short description of the class.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Recursion {
//	public static void main(String[] args) {
//		testSum();
//		testFindMin();
//		testFindSum();
//	}
	
	public static int sum(int n) {
		if (n <= 1) {
			return n;
		}
		
		return n + sum(n - 1);
	}
	
	public static int findMin(int[] a) {
		validateArrayNotEmpty(a);
		return findMin(a, a.length);
	}

	private static int findMin(int[] a, int n) {
		final int base = 1;
		if (n == base) {
			return a[a.length - n];
		}
		
		return min(findMin(a, n - base), a[a.length - n]);
	}

	public static int findSum(int[] a) {
		validateArrayNotEmpty(a);
		return findSum(a, a.length);
	}

	private static int findSum(int[] a, int n) {
		final int base = 1;
		if (n <= base) {
			return a[a.length - n];
		}
		
		return findSum(a, n - base) + a[a.length - n];
	}
	
	private static void validateArrayNotEmpty(int[] a) {
		if (a.length == 0) {
			throw new IllegalArgumentException("Array should not be empty.");
		}
	}
	
//	// demonstrates the sum() function for values of 1..10
//	private static void testSum() {
//		for (int i = 1; i <= 10; ++i) {
//			System.out.printf("sum(%d) = %d%n", i, sum(i));
//		}
//	}
//
//	// tests the findMin() function for zero, one, two and three element arrays
//	private static void testFindMin() {
//		int[] a = {};
//		String fnName = "findMin";
//		testCallWithEmpty(a, Recursion::findMin, fnName);
//		
//		int[][] testCases = {
//			{1},
//			{1,2},
//			{2,1},
//			{0, 1, 2},
//			{0, 2, 1},
//			{1, 0, 2},
//			{1, 2, 0},
//			{2, 0, 1},
//			{2, 1, 0},
//		};
//		
//		for (int[] z : testCases) {
//			System.out.printf(
//					"findMin(%s) = %d%n",
//					Arrays.toString(z),
//					findMin(z));
//		}
//	}
//
//	// Tests findSum() for zero, one, two, and three element arrays.
//	private static void testFindSum() {
//		String fnName = "findSum";
//		int[] a = {}; // no elems should throw ArrayIndexOutOfBoundsException
//		testCallWithEmpty(a, Recursion::findSum, fnName);
//		int[] b = {5}; // should return 5
//		int[] c = {2,5}; // should return 7
//		int[] d = {3,6,9}; // should return 18
//		
//		for (int[] z : new int[][] {b,c,d}) {
//			System.out.printf(
//					"findSum(%s) = %d%n",
//					Arrays.toString(z),
//					findSum(z));
//		}
//	}
//
//	// Logs a failure if an exception is not generated as a result of calling
//	// fn with an empty array.
//	private static void testCallWithEmpty(
//			int[] a,
//			Function<int[], Integer> fn,
//			String fnName) {
//		try {
//			fn.apply(a);
//			System.err.printf("FAIL: %s(%s) did not produce an "
//					+ "exception.%n",
//					fnName,
//					a);
//		} catch(IllegalArgumentException e) {
//			System.out.printf("%s(%s) threw IllegalArgumentException as "
//					+ "expected.%n",
//					fnName,
//					Arrays.toString(a));
//		}
//	}
	
}
