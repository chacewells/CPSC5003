/*
 * Aaron Wells
 * CPSC 5002, Seattle University
 * This is free and unencumbered software released into the public domain.
 */
package huffman;

import java.util.PriorityQueue;

/**
 * I'm a Huffman.
 *
 * @author  Aaron Wells
 * @version 1.0
 */
public class Huffman {
	public Huffman(String sample, boolean verbose) {
		int[] counts = countSample(sample);
		makeCodeTree(counts);
		makeEncodeMap();
	}
	
	private void makeEncodeMap() {
		encodeMap = new BitStream[BIT_PATTERNS_PER_BYTE];
		BitStream bs = new BitStream();
		makeEncodeMap(root, bs);
	}
	
	private void makeEncodeMap(Node curr, BitStream prefix) {
		if (curr != null) {
			if (curr.type == NodeType.LEAF) {
				encodeMap[curr.value] = prefix;
			} else {
				makeEncodeMap(curr.left, prefix);
				makeEncodeMap(curr.right, prefix);
			}
		}
	}
	
	public BitStream encode(String uncompressed) {
		BitStream compressed = new BitStream();
		for (char c : uncompressed.toCharArray())
			compressed.append(encodeMap[c]);
		return compressed;
	}

	public String decode(BitStream compressed) {
		StringBuilder decompressed = new StringBuilder();
		Node curr = root;
		while (!compressed.empty()) {
			boolean bit = compressed.nextBit();
			curr = (bit) ? curr.right : curr.left;
			if (curr.type == NodeType.LEAF) {
				decompressed.append(curr.value);
				curr = root;
			}
		}
		return decompressed.toString();
	}

	private void makeCodeTree(int[] counts) {
		PriorityQueue<Node> pq = new PriorityQueue<>(
				(a, b) -> a.frequency - b.frequency
		);
		for (int c = 0; c < counts.length; ++c)
			if (counts[c] > 0)
				pq.offer(new Node(c, counts[c]));
		
		Node redLantern = null;
		while (!pq.isEmpty()) {
			redLantern = pq.poll(); // 'poll' is 'dequeue'
			if (pq.isEmpty())
				break;
			Node runnerUp = pq.poll();
			pq.offer(new Node(redLantern, runnerUp));
		}
		
		root = redLantern;
	}

	private int[] countSample(String sample) {
		int[] counts = new int[BIT_PATTERNS_PER_BYTE];
		for (char c : sample.toCharArray())
			counts[c]++;
		return counts;
	}
	
	private static final int BIT_PATTERNS_PER_BYTE = 256; // number of possible 8-bit things
	
	private Node root;
	private BitStream[] encodeMap;
	
	private enum NodeType { LEAF, INTERIOR }
	
	private class Node {
		public NodeType type;
		public char value;
		public int frequency;
		public Node left, right;
		
		public Node(int c, int freq) {
			type = NodeType.LEAF;
			frequency = freq;
			value = (char)c;
		}
		
		public Node(Node l, Node r) {
			type = NodeType.INTERIOR;
			frequency = l.frequency + r.frequency;
			
			left = l;
			right = r;
		}
	}
}
